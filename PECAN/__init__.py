#
# Ensure the user is running the version of python we require
import sys as _sys
if not hasattr(_sys, "version_info") or _sys.version_info < (2,7):
    raise RuntimeError( "PECAN requires Python 2.7 or later.")

del _sys

__all__ = ['Peptide',
           'TheoreticalGenerator',
           'Chromatogram',
           'Scorer',
           'PecanUtil']

from PECAN import *



#!/usr/bin/env python 

import csv
import logging
import random
import os
import numpy as np
import collections
from copy import deepcopy
from .Peptide import *

logger = logging.getLogger('PECAN.TheoreticalGenerator')


class PrecursorIndexer(object):
    def __init__(self, targetPeptides):
        mzNeeded = {}                  # key = b/y-ion mz, value = counts
    
        for peptide in targetPeptides.GetPeptideList():
            isotopeDist = peptide.GetIsotopeDist()
            logger.debug('targetSeq: %s\tisotopeDist: %s', peptide.GetSequence(), isotopeDist)
            
            for pmz in isotopeDist.keys():
                mzNeeded[pmz] = 1
        
        self._mzList = sorted(mzNeeded.keys())

    def GetMzList(self):        
        return self._mzList


class FragmentIndexer(object):
    def __init__(self, mzBoundaries, targetPeptides, uniBG,
                 tolerance=10,
                 unit='ppm',
                 bgProteomePeptideList=None,
                 ionTypes=('b', 'y')):

        MzInfo = collections.namedtuple('MzInfo', 'weight probability type')
        self._mzInfos = {}
        self._unit = unit
        mzNeeded = {}                  # key = b/y-ion mz, value = 1
        mzFrequency = {}               # key = b/y-ion mz, value = counts
        mzType = {}                    # key = mz, value = i for b/y-ions and e for background estimated only 

        self._decoyPeptides = PeptideList()
        totalTrans = 0.0
        if bgProteomePeptideList is None:
            bgProteomePeptideList = targetPeptides.GetPeptideList()

        # calculating transition m/z frequency from background proteome
        targetPeptideSequenceList = targetPeptides.GetPeptideSequenceList()
        for bgpPeptide in bgProteomePeptideList:
            # bgpPeptideSeq = bgpPeptide.GetSequence()
            # if bgpPeptideSeq not in targetPeptideSequenceList:
            targetMzs = bgpPeptide.GetFragmentIonMz(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
            for tmz in targetMzs:
                totalTrans += 1
                if tmz in mzFrequency:
                    mzFrequency[tmz] += 1
                else:
                    mzFrequency[tmz] = 1
                    mzType[tmz] = 'f'

        # add in mz needed for uniBG decoys
        bgMzNeeded = uniBG.GetMzNeeded()
        for tmz in bgMzNeeded:
            totalTrans += 1
            mzNeeded[tmz] = 1
            mzType[tmz] = 'e'
            '''
            # uniBG decoy peptides are not considered in the mz frequency for mz weight
            if tmz in mzFrequency:
                mzFrequency[tmz] += bgMzNeeded[tmz]
            else:
                mzFrequency[tmz] = bgMzNeeded[tmz]
            '''
        # add in mz needed for query targets and decoys
        dGenerator = DecoyGenerator(bgProteomePeptideList, mzBoundaries=mzBoundaries)
        for peptide in targetPeptides.GetPeptideList():
            targetMzs = peptide.GetFragmentIonMz(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
            decoy = dGenerator.GetSmartDecoy(peptide)
    
            self._decoyPeptides.AddAPeptide(decoy)
            decoyMzs = decoy.GetFragmentIonMz(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
            logger.debug('targetSeq: %s\tdecoySeq: %s', peptide.GetSequence(), decoy.GetSequence())
            logger.debug('%s: %s', peptide.GetID(), peptide.GetFragmentIonMz())
            logger.debug('%s: %s', decoy.GetID(), decoy.GetFragmentIonMz())

            for tmz in targetMzs+decoyMzs:
                totalTrans += 1
                mzNeeded[tmz] = 1
                mzType[tmz] = 'i'
                '''
                # target/decoy query peptides are not considered in the mz frequency for mz weight
                if tmz in mzFrequency:
                    mzFrequency[tmz] += 1
                else:
                    mzFrequency[tmz] = 1
                '''
        
        maxFreq = float(max(mzFrequency.values()))
        logger.info('Most common ion shows up %d time', maxFreq)
        
        self._mzList = sorted(mzNeeded.keys())
        mzFrequencyList = [(k, mzFrequency[k]) for k in sorted(mzFrequency.keys())]
        
        tmzInstances = []
        lowerMzs = []
        upperMzs = []
        for mz in self._mzList:
            if self._unit == 'ppm':
                massErr = mz * tolerance *(10**-6)
            elif self._unit == 'mz':
                massErr = tolerance
            else:
                logger.warning('Unknown unit for extraction %s\nRevert to ppm\n', self._unit)
                massErr = mz * tolerance * (10**-6)

            tmz = TMZ(mz)
            tmzInstances.append(tmz)
            lowerMzs.append(mz-massErr)
            upperMzs.append(mz+massErr)

        mzExtract = MZExtracter(tmzInstances, lowerMzs, upperMzs)
        mzExtract.Extract(mzFrequencyList)
        tmzInstances = mzExtract.GetResult()
        
        for tmz in tmzInstances:
            self._mzInfos[tmz.GetMz()] = MzInfo(weight=tmz.GetIonIntensity(),
                                                probability=tmz.GetProbability(totalTrans=totalTrans),
                                                type=mzType[tmz.GetMz()])
            
            logger.debug('tmz: %s  freq: %s\tintensity: %.4f\tprob: %s\ttype: %s', 
                         tmz.GetMz(), tmz.GetValue(), tmz.GetIonIntensity(),
                         tmz.GetProbability(totalTrans=totalTrans), mzType[tmz.GetMz()])
        ''' all the TMZ objects should be gone by now'''
        del tmz, tmzInstances
        del mzFrequency, mzFrequencyList, bgProteomePeptideList, targetPeptideSequenceList, bgMzNeeded, mzType
    
    def GetList(self):        
        return self._mzList
    
    def GetMzType(self, mz):
        return self._mzInfos[mz].type

    def GetMzWeight(self, mz):
        return self._mzInfos[mz].weight

    def GetMzProbability(self, mz):
        return self._mzInfos[mz].probability

    def GetDecoyPeptides(self):
        return deepcopy( self._decoyPeptides)


class UniBGMtxGenerator:
    # this class keep the scores of N decoys separated for scoring
    # to calculate the mean and std of decoys over time
    def __init__(self, mzBoundaries, targetPeptides, bgDecoyNumber=2000, ionTypes=('b', 'y')):
        if targetPeptides.CheckPeptideByChargeSize(bgDecoyNumber):
            # Not enough peptides in the bgProteome to make decoys
            # create a new peptideList object only for bg decoys
            backupList = deepcopy(targetPeptides)
            i = 0
            while backupList.CheckPeptideByChargeSize(bgDecoyNumber):
                bgFileName = os.path.join(os.path.dirname(__file__), "PecanUtil", "backup_proteome_%s.txt" % i)
                backupList.LoadFromFile(bgFileName, ignoreProtein=True)
                i += 1
            peptideList = backupList.GetPeptideList()
            peptideByCharge = backupList.GetPeptideByCharge()
        else:
            peptideList = targetPeptides.GetPeptideList()
            peptideByCharge = targetPeptides.GetPeptideByCharge()

        self._bgMtxByCharge = {}    # key = charge, value = [] of decoy fragmentation tuples (b2,...,y2,...,yn-1)
        self._bgMzNeeded = {}       # key = mz, value = frequency
        self._bgCharges = []        # charges to calculate bg scores for
        self._scoreMtx = {}         # key = charge, value = array of scores
        self._scoreMtxStd = {}      # key = charge, value = array of standard deviations
        dGenerator = DecoyGenerator(peptideList, mzBoundaries=mzBoundaries)

        for charge in peptideByCharge.keys():
            self._bgMtxByCharge[charge] = []
            self._bgCharges.append(charge)

            random.seed(charge)
            for peptide in random.sample(peptideByCharge[charge], bgDecoyNumber):
                decoyMzs = dGenerator.GetDecoy(peptide).GetFragmentIonMz(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
                self._bgMtxByCharge[charge].append(decoyMzs)
                for mz in decoyMzs:
                    if mz in self._bgMzNeeded:
                        self._bgMzNeeded[mz] += 1
                    else:
                        self._bgMzNeeded[mz] = 1
            logger.info('Created +%s background with %s decoys', charge, bgDecoyNumber)

    def GetMzList(self):
        return sorted(self._bgMzNeeded.keys())

    def GetMzNeeded(self):
        return self._bgMzNeeded

    def CalScoreMtx(self, ms2):
        chromSize = ms2.GetChromatogramSize()
        for charge in self._bgCharges:
            bgScores = []
            for decoyFragments in self._bgMtxByCharge[charge]:
                # to prevent divide by zero
                decoyScore = np.ones(chromSize)
                theoSpectrum = []
                for mz in decoyFragments:
                    theoSpectrum.append(ms2.GetMzWeight(mz))
                    decoyScore += ms2.GetMzScoreMtx(mz)

                decoyNorm = np.linalg.norm(theoSpectrum)
                bgScores.append(decoyScore/decoyNorm)

            '''
            tmpFile = 'bgScoresMtx.charge_%s_%s.tsv' % (charge, len(bgScores))
            np.savetxt(tmpFile, np.vstack(bgScores).transpose(),
                       delimiter='\t',
                       fmt='%1.4f')
            '''

            self._scoreMtx[charge] = np.mean(bgScores, 0)
            self._scoreMtxStd[charge] = np.std(bgScores, 0)

        del ms2, bgScores

    def GetChargeScoreMtx(self, charge):
        return self._scoreMtx[charge]

    def GetChargeScoreStd(self, charge):
        return self._scoreMtxStd[charge]


class MZExtracter(object):
    def __init__(self, tmzInstances, lowerMzs=list(), upperMzs=list()):
        self._tmzInstances = tmzInstances    
        self._tmzNumber = len(tmzInstances)
        self._lowerMzs = lowerMzs
        self._upperMzs = upperMzs
    
    def Extract(self, mzList, **kwarg):    
        tmzIndex = 0
        mzIndex = 0
        while tmzIndex < self._tmzNumber and mzIndex < len(mzList):
            (mz, value) = mzList[mzIndex]
            tmz = self._tmzInstances[tmzIndex]  # tmz = a TMZ instance

            if self._lowerMzs[tmzIndex] <= mz < self._upperMzs[tmzIndex]:
                # do something about function
                tmz.AddValue(mz, value, **kwarg)
                #
                tmzLocalIndex = tmzIndex + 1
                while tmzLocalIndex < self._tmzNumber:
                    tmzLocal = self._tmzInstances[tmzLocalIndex]
                    if self._lowerMzs[tmzLocalIndex] <= mz < self._upperMzs[tmzLocalIndex]:
                        # do it again
                        tmzLocal.AddValue(mz, value, **kwarg)
                        #
                        tmzLocalIndex += 1
                    elif mz < self._lowerMzs[tmzLocalIndex]:
                        break
                    else:
                        logger.warning("something wrong")
                mzIndex += 1 
            elif mz < self._lowerMzs[tmzIndex]:
                mzIndex += 1
            elif mz >= self._upperMzs[tmzIndex]:
                tmzIndex += 1
            else:
                logger.warning("unexpected case")

    def GetResult(self):
        return deepcopy(self._tmzInstances)


class TMZ(object):
    def __init__(self, mz=0, frequency=1):
        self._mz = mz
        self._frequency = frequency     # add a pseudo count 1
        self._probability = None
        self._ionIntensity = None
        self._tType = None

    def GetType(self):
        return self._tType

    def GetMz(self):
        return self._mz
    
    def AddValue(self, mz, value):
        self._frequency += value
    
    def GetValue(self):
        return self._frequency
    
    def GetProbability(self, totalTrans=1):
        if self._probability is None:
            self._probability = self._frequency/float(totalTrans)            
        return self._probability 
    
    def GetIonIntensity(self):
        if self._ionIntensity is None:
            self._ionIntensity = 100.0/self._frequency
        return self._ionIntensity


class PeptideList(object):
    def __init__(self, listOfPeptides=None, minPrecursor=200.0, maxPrecursor=2000.0,
                 minChargeState=2, maxChargeState=3, fixedMods=None):
        self._peptideIDs = None
        self._minPrecursor = minPrecursor
        self._maxPrecursor = maxPrecursor
        self._minChargeState = minChargeState
        self._maxChargeState = maxChargeState
        self._fixedMods = fixedMods
        self._peptideByCharge = {}
        for charge in xrange(self._minChargeState, self._maxChargeState+1):
            self._peptideByCharge[charge] = []

        if listOfPeptides is None:
            self._peptideList = []
            self._peptideNumber = 0
            self._peptideSequences = []
        else:
            self._peptideList = deepcopy(listOfPeptides)
            self._peptideNumber = len(listOfPeptides)
            self._peptideSequences = [peptide.GetSequence() for peptide in listOfPeptides]

    def AddAPeptide(self, peptide):
        self._peptideList.append(peptide)
        self._peptideNumber += 1
        self._peptideSequences.append(peptide.GetSequence())

    def FixedModPeptide(self, sequence):
        for aa in self._fixedMods:
            sequence = re.sub('%s(\[.+\])?' % aa, self._fixedMods[aa], sequence)
        return sequence

    def LoadFromFile(self, peptideListFileName, ignoreProtein=False):
        logger.info("====== FILTERING PEPTIDES: %s ======", peptideListFileName)
        logger.info("Filtering: %.4f - %.4f", self._minPrecursor, self._maxPrecursor)

        numPeptides = 0
        peptideSequences = {}   # key = peptideSeq, value = Peptide object
        peptideByCharge = {}    # key = charge, value = [] of peptide sequence with that charge

        for charge in xrange(self._minChargeState, self._maxChargeState+1):
            peptideByCharge[charge] = []

        with open(peptideListFileName, 'rb') as f:
            dialect = csv.Sniffer().sniff(f.readline(), ['\t', ' ', ','])
            f.seek(0)
            for row in csv.DictReader(f, delimiter=dialect.delimiter):
                sequence = self.FixedModPeptide(row.get('Sequence'))
                proteinName = row.get('Protein_Name')
                numPeptides += 1

                for charge in xrange(self._minChargeState, self._maxChargeState+1):
                    peptide = Peptide(sequence, charge, proteinNames=[proteinName])
                    precursorMz = peptide.GetPrecursorMz()
                    if self._minPrecursor <= precursorMz < self._maxPrecursor:
                        peptideByCharge[charge].append(peptide)
                        if ignoreProtein:
                            peptideSequences[sequence] = peptide
                        else:
                            if sequence in peptideSequences:
                                peptideSequences[sequence].AddAProteinName(proteinName)
                            else:
                                peptideSequences[sequence] = peptide
                    else:
                        del peptide
        
        listOfPeptides = peptideSequences.values()  # a list of Peptide objects
        numStored = len(listOfPeptides)

        logger.info("Found %d peptides total and %d peptides stored", numPeptides, numStored)
        if numStored == 0:
            logger.info("%d query peptides. Quit now", numStored)
            sys.exit()

        for charge in peptideByCharge.keys():
            if len(peptideByCharge[charge]) == 0:
                peptideByCharge.pop(charge, None)
            else:
                self._peptideByCharge[charge] += peptideByCharge[charge]
        
        self._peptideNumber += numStored
        self._peptideList += listOfPeptides
        self._peptideSequences += peptideSequences.keys()

    def GetPeptideNumber(self):
        return self._peptideNumber
   
    def GetPeptideList(self):
        return self._peptideList

    def GetPeptideByCharge(self):
        return self._peptideByCharge
    
    def GetPeptideSequenceList(self):
        return self._peptideSequences   

    def GetPeptideIDList(self):
        if self._peptideIDs is None:
            self._peptideIDs = []
            for peptide in self._peptideList:
                self._peptideIDs.append(peptide.GetID())
        return self._peptideIDs

    def CheckPeptideByChargeSize(self, size):
        needMore = False
        for charge in self._peptideByCharge.keys():
            n = len(self._peptideByCharge[charge])
            if n < size:
                logger.info('%s peptide with charge %s. Less than %s', n, charge, size)
                needMore = True
        return needMore

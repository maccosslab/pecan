#!/usr/bin/env python
import logging
import numpy as np
import operator
from collections import namedtuple
from datetime import datetime
from copy import deepcopy
from struct import pack

# import matplotlib as mpl
# import matplotlib.pyplot as plt

logger = logging.getLogger('PECAN.Scorer')
PeakCIons = namedtuple('PeakCIons', 'peakId, peakMZ, peakIntensity')


def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, [0.0]))
    return (cumsum[N:]-cumsum[:-N])/float(N)


class Feature(object):
    def __init__(self, peptide, **kwargs):
        self._peptide = peptide
        self._peakIdentifier = None
        if self._peptide.GetCharge() == 2:
            self._charge2 = 1
            self._charge3 = 0
        elif self._peptide.GetCharge() == 3:
            self._charge2 = 0
            self._charge3 = 1
        else:
            self._charge2 = 0
            self._charge3 = 0

        for k, v in kwargs.items():
            setattr(self, k, v)

    def GetID(self):
        if self._peakIdentifier is None:
            self._peakIdentifier = '%s:%.2f:%s' % \
                                   (getattr(self, 'rawFileName'), getattr(self, 'midTime'), self._peptide.GetID())
        return self._peakIdentifier

    def Add(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def GetTime(self):
        return getattr(self, 'midTime')
        
    def SetTime(self, startTime, endTime):
        setattr(self, 'duration', (endTime-startTime)*60)
        setattr(self, 'midTime', (startTime+endTime)/2.0)

    def GetCIons(self):
        cIonsScoreArray = np.array(getattr(self, 'cIonsScores')).transpose()
        peakMZ = cIonsScoreArray[0]
        peakInt = cIonsScoreArray[1]
        peakSize = len(peakMZ)
        mzBlob = pack('d'*peakSize, *peakMZ)
        intBlob = pack('f'*peakSize, *peakInt)
        return PeakCIons(self.GetID(), buffer(mzBlob), buffer(intBlob))

    def PrintFeatures(self, featureFile):
        featureFile.write("\n%s" % self.GetID())
        featureFile.write("\t%s" % self._peptide.GetTag())
        featureFile.write("\t%d" % getattr(self, 'scanNr'))
        #
        featureFile.write("\t%s" % getattr(self, 'peakTopx'))
        featureFile.write("\t%s" % getattr(self, 'peakRank'))
        featureFile.write("\t%.4f" % getattr(self, 'peakZScore'))
        featureFile.write("\t%.4f" % getattr(self, 'peakBGSScore'))
        featureFile.write("\t%.4f" % getattr(self, 'deltaSn'))
        featureFile.write("\t%.4f" % getattr(self, 'peakAvgIdotp'))
        featureFile.write("\t%.4f" % getattr(self, 'peakMidIdotp'))
        featureFile.write("\t%.4f" % getattr(self, 'peakScore'))
        featureFile.write("\t%.4f" % getattr(self, 'peakWScore'))
        featureFile.write("\t%d" % getattr(self, 'peakIons'))
        featureFile.write("\t%.2f" % getattr(self, 'peakMassErrMean'))
        featureFile.write("\t%.2f" % getattr(self, 'peakMassErrVar'))
        featureFile.write("\t%.2f" % getattr(self, 'precursorMassErrMean'))
        featureFile.write("\t%.2f" % getattr(self, 'precursorMassErrVar'))
        featureFile.write("\t%.4f" % getattr(self, 'peakSimilarity'))
        featureFile.write("\t%.2f" % getattr(self, 'duration'))
        featureFile.write("\t%.2f" % getattr(self, 'midTime'))
        featureFile.write("\t%.2f" % getattr(self, 'spectraNorm'))
        featureFile.write("\t%d" % self._peptide.GetLength())
        featureFile.write("\t%d\t%d" % (self._charge2, self._charge3))
        featureFile.write("\t-.%s.-" % self._peptide.GetModSequence())
        featureFile.write("\t%s" % self._peptide.GetProteinID())


class RetentionTimeMapper(object):
    def __init__(self, ms1RetentionTimes, ms2Retentiontimes):
        self._ms2NumElutions = len(ms2Retentiontimes)
        self._rtMap = {}  # key = ms2Idx, value = ms1Idx (the latest MS1 before the MS2 by retention time)

        ms1LastIdx = 0
        for ms2Idx in xrange(0, self._ms2NumElutions):
            ms2Time = ms2Retentiontimes[ms2Idx]
            for ms1Idx in xrange(ms1LastIdx, len(ms1RetentionTimes)):
                ms1Time = ms1RetentionTimes[ms1Idx]
                if ms1Time <= ms2Time:
                    continue
                else:
                    '''the first ms1 after the ms2 time'''
                    if ms1Idx == 0:
                        ms1Idx += 1
                    self._rtMap[ms2Idx] = ms1Idx-1
                    ms1LastIdx = ms1Idx-1
                    break
            if ms2Idx not in self._rtMap:
                self._rtMap[ms2Idx] = len(ms1RetentionTimes)-1

    def GetMS1Idxs(self, ms2TimeTag):
        start = ms2TimeTag[0]
        end = ms2TimeTag[1]
        if end >= (self._ms2NumElutions-1):
            end = (self._ms2NumElutions-1)
        return self._rtMap[start], self._rtMap[end]


class FeatureGenerator(object):
    def __init__(self, ms1, ms2, rtMapper, queryPeptides, uniBG, minElute, maxElute, idotpThreshold,
                 ms1Tolerance, ms2Tolerance, topX, alpha, beta):
        self._ms1 = ms1
        self._ms2 = ms2
        self._rtMapper = rtMapper
        self._queryPeptides = queryPeptides
        self._minElute = minElute
        self._maxElute = maxElute
        self._ionMatchFraction = beta
        self._idotpThreshold = idotpThreshold
        self._ms1Tolerance = ms1Tolerance
        self._ms2Tolerance = ms2Tolerance
        self._topX = topX
        self._alpha = alpha
        self._PecanPeak = namedtuple('PecanPeak',
                                     'BGSScore, score, start, end, ionMatch, massErrMean, massErrVar,'
                                     'avgIdotp, midIdotp, topx, rank, cIonsScores')

        self._totalNumPeptide = queryPeptides.GetPeptideNumber()*2 
        self._resultList = []

        pepTime = datetime.now()
       
        PecanResult = namedtuple('PecanResult', 'peptide pepFeature')

        self._ms1NumElutions = len(ms1.GetRetentionTimes())
        self._ms2NumElutions = len(ms2.GetRetentionTimes())

        #row = 0
        #totalRows = queryPeptides.GetPeptideNumber()
        #plt.figure(figsize=(12, 12), dpi=100)
        #grid_size = (totalRows, 1)
        
        for peptide in queryPeptides.GetPeptideList():
            self._peptide = peptide
            self._numComp = len(self._peptide.GetFragmentIonMz())
            self._minIonMatch = int(self._numComp * self._ionMatchFraction)

            ''' idotp calculation '''
            theoSpectrum = []
            dotp = np.zeros(self._ms1NumElutions)
            sumSquare = np.zeros(self._ms1NumElutions)
             
            for mz in sorted(peptide.GetIsotopeDist()):
                mtx = ms1.GetMzMtx(mz)
                theoSpectrum.append(peptide.GetIsotopeDist()[mz])
                dotp += mtx * peptide.GetIsotopeDist()[mz]                
                sumSquare += mtx ** 2
           
            sumSquare = np.sqrt(sumSquare)
            sumSquare[np.where(sumSquare == 0)[0]] = 1.0
            iNorm = np.linalg.norm(theoSpectrum)
            normDotp = dotp/(sumSquare*iNorm)

            ''' ms2 dot protuct calculation '''
            theoSpectrum = []
            pepScore = np.zeros(self._ms2NumElutions)
            pepWScore = np.zeros(self._ms2NumElutions)
            
            for mz in peptide.GetFragmentIonMz():
                theoSpectrum.append(ms2.GetMzWeight(mz))
                pepScore += ms2.GetMzScoreMtx(mz)
                pepWScore += ms2.GetMzScoreMtx(mz)*mz 

            pepNorm = np.linalg.norm(theoSpectrum)
            pepScore /= pepNorm
            pepWScore /= pepNorm

            pepBgSubtractedScoreMtx = pepScore - uniBG.GetChargeScoreMtx(peptide.GetCharge())
            pepZscoreMtx = (pepScore-uniBG.GetChargeScoreMtx(peptide.GetCharge()))/uniBG.GetChargeScoreStd(peptide.GetCharge())
            cosines = pepScore/(np.array(ms2.GetNorms())+0.000001)

            #for i in xrange(0, self._ms2NumElutions):
            #    logger.debug('#pepScore: %s', pepScore[i])
            #for i in xrange(0, self._ms2NumElutions):    
            #    logger.debug('#uniBG: %s', uniBG.GetChargeScoreMtx( peptide.GetCharge() )[i])

            #plt.subplot2grid(grid_size, (row,0), rowspan=1, colspan=1)
            #plt.title('peptide: %s' % peptide.GetID() )
            #plt.plot(pepScore, 'o', label='pepScore')
            #plt.plot(uniBG.GetChargeScoreMtx( peptide.GetCharge()), 'o', label='uniBG')
            #plt.plot(pepBgSubtractedScoreMtx, 'o', label='BGS') 
            #row +=1

            ''' pick peaks from highest ranked BGSscore '''
            peaks, deltaScores = self.PickPeak(normDotp, pepBgSubtractedScoreMtx, pepScore, pepNorm)

            for peak in peaks:
                peakFeature = Feature(peptide)
                if peak.start == peak.end: 
                    pass
                else:
                    peakWScore = np.average(pepWScore[peak.start:peak.end] )
                    peakSimilarity = np.average(cosines[peak.start:peak.end])
                    peakStartTime = ms2.GetRetentionTimes()[peak.start]
                    peakEndTime = ms2.GetRetentionTimes()[peak.end-1]
                
                    ''' calculating precursor massErr mean and variance'''
                    precursorMassErrors = []
                    for mz in sorted(peptide.GetIsotopeDist()):
                        ms1StartIdx, ms1EndIdx = self._rtMapper.GetMS1Idxs((peak.start, peak.end))
                        precursorIntensities = ms1.GetMzMtx(mz)[ms1StartIdx:ms1EndIdx]
                        if sum(precursorIntensities) == 0:
                            continue    
                        logger.debug('precursorIntensities [%s %s]= %s\n', ms1StartIdx, ms1EndIdx, precursorIntensities)
                        weightedPrecursorMassError = sum(np.multiply(ms1.GetMassErrMtx(mz)[ms1StartIdx:ms1EndIdx],
                                                                     precursorIntensities)/sum(precursorIntensities))
                        precursorMassErrors.append(weightedPrecursorMassError)
                    
                    if len(precursorMassErrors) == 0:
                        precursorMassErrMean = ms1Tolerance
                        precursorMassErrVar = ms1Tolerance**2
                    else:
                        precursorMassErrMean = np.mean(precursorMassErrors)
                        precursorMassErrVar = np.var(precursorMassErrors)
                        
                    logger.debug('precursorMassErrMean = %s\tprecursorMassErrVar = %s\n', precursorMassErrMean, precursorMassErrVar)
                    deltaScore, deltaSn = deltaScores.pop(0)
                    peakFeature.Add(peakBGSScore=peak.BGSScore,
                                    peakScore=peak.score,
                                    peakWScore=peakWScore,
                                    peakZScore=np.average(pepZscoreMtx[peak.start:peak.end]),
                                    peakSimilarity=peakSimilarity,
                                    peakAvgIdotp=peak.avgIdotp,
                                    peakMidIdotp=peak.midIdotp,
                                    deltaSn=deltaSn,
                                    peakIons=peak.ionMatch,
                                    peakTopx=peak.topx,
                                    peakRank=peak.rank,
                                    pepNorm=pepNorm,
                                    spectraNorm=np.average(ms2.GetNorms()[peak.start:peak.end]),
                                    peakMassErrMean=peak.massErrMean,
                                    peakMassErrVar=peak.massErrVar,
                                    precursorMassErrMean=precursorMassErrMean,
                                    precursorMassErrVar=precursorMassErrVar)
                    peakFeature.Add(scanNr=ms2.GetScanNumber(int((peak.end+peak.start)/2)))
                    peakFeature.Add(cIonsScores=peak.cIonsScores)
                    peakFeature.SetTime(peakStartTime, peakEndTime)
            
                    self._resultList.append(PecanResult(peptide=peptide, pepFeature=peakFeature))

            deltaTime = datetime.now() - pepTime
            pepTime = datetime.now()
            logger.debug('===== PEP %s: %s micro-seconds =====', peptide.GetID(), deltaTime.microseconds)
        #plt.savefig('scores_%s.png' %td) 

    def GetResult(self):
        return deepcopy(self._resultList)
    
    def PickPeak(self, normDotp, pepBgSubtractedScoreMtx, pepScore, pepNorm):
        avgScores = {}      # key=time, value = score
        avgIsoDotps = {}
        midIsoDotps = {}
        peaks = []
        deltaScores = []

        '''Consider all valid start/end combinations for only the minEluteTime'''
        avgS = running_mean(pepBgSubtractedScoreMtx, self._minElute)

        for ms2StartIdx in xrange(0, avgS.size-self._minElute+1):
            ''' ms1 retention time look up '''
            time = (ms2StartIdx, ms2StartIdx+self._minElute)
            ms1StartIdx, ms1EndIdx = self._rtMapper.GetMS1Idxs(time)
            ms1Span = ms1EndIdx - ms1StartIdx
            if ms1Span == 0:
                avgIsoDotp = 0.0
                midIsoDotp = 0.0
            else:
                regionIsoDotps = normDotp[ms1StartIdx:ms1EndIdx]
                avgIsoDotp = np.average(regionIsoDotps)
                if ms1Span % 2 == 0:
                    midIsoDotp = (regionIsoDotps[ms1Span/2] + regionIsoDotps[(ms1Span/2) - 1])/2
                else:
                    midIsoDotp = regionIsoDotps[ms1Span/2]

            ''' ms1 idotp passes criteria '''
            if self._idotpThreshold == 0.0:
                avgScores[time] = avgS[ms2StartIdx]
                avgIsoDotps[time] = avgIsoDotp
                midIsoDotps[time] = midIsoDotp
            elif midIsoDotp >= self._idotpThreshold and avgIsoDotp >= self._idotpThreshold/2:
                avgScores[time] = avgS[ms2StartIdx]
                avgIsoDotps[time] = avgIsoDotp
                midIsoDotps[time] = midIsoDotp

        if len(avgScores.keys()) == 0:
            ''' nothing passes ms1 filtering '''
            return peaks, deltaScores
        else:
            rankList = sorted(avgScores.iteritems(), key=operator.itemgetter(1), reverse=True)

            peakCount = 0
            peakBoundaries = []
            for rank in xrange(0, len(rankList)):
                if peakCount > self._topX:
                    break
                else:
                    rankTime, rankScore = rankList[rank]
                    startRTIndex, endRTIndex = rankTime
                    midRTIndex = (startRTIndex+endRTIndex)/2
                    exclude = False
                    for exStart, exEnd in peakBoundaries:
                        if exStart <= midRTIndex < exEnd:
                            logger.debug('Exclude %s within PecanPeaks %s', rankTime, peakBoundaries)
                            exclude = True
                            break
                    
                    if exclude:
                        continue
                    else:
                        rawScore = np.sum(pepScore[startRTIndex:endRTIndex])
                        isPeak, peakIonMatch, massErrMean, massErrVar, cIonsScores = self.IfIsPeak(startRTIndex,
                                                                                                   endRTIndex,
                                                                                                   rawScore,
                                                                                                   pepScore[startRTIndex:endRTIndex],
                                                                                                   pepNorm,
                                                                                                   fast=False)
                        logger.debug('IfIsPeak: %s\t%s\tcIons: %s\tmassErrMean: %s\tmassErrVar:%s\tavgIdotp: %s\tmid: %s',
                                     rank, rankList[rank], peakIonMatch, massErrMean, massErrVar, avgIsoDotps[rankTime], midIsoDotps[rankTime])
                        if isPeak:
                            peakCount += 1
                            peakBoundaries.append((startRTIndex-self._minElute, endRTIndex+self._minElute))
                            thisPeak = self._PecanPeak(BGSScore=rankScore,
                                                       score=rawScore/(endRTIndex-startRTIndex),
                                                       start=startRTIndex,
                                                       end=endRTIndex,
                                                       ionMatch=peakIonMatch,
                                                       massErrMean=massErrMean,
                                                       massErrVar=massErrVar,
                                                       avgIdotp=avgIsoDotps[rankTime],
                                                       midIdotp=midIsoDotps[rankTime],
                                                       topx=peakCount,
                                                       rank=rank,
                                                       cIonsScores=cIonsScores)
                            peaks.append(thisPeak)
                            logger.debug('= TOP %s Peak: %s\t%s\tcIons: %s\tmassErrMean: %s\tmassErrVar:%s\tavg idotp: %s\tmid idotp: %s',
                                      peakCount, rank,  rankList[rank], thisPeak.ionMatch, thisPeak.massErrMean, thisPeak.massErrVar, thisPeak.avgIdotp, thisPeak.midIdotp)

        '''deltaScores calcultiaon '''
        for i in xrange(0, len(peaks)):
            if peaks[i].BGSScore == 0.0:
                deltaScores.append((0.0, 0.0))
                continue
            try:
                delta = peaks[i].BGSScore - peaks[i+1].BGSScore
            except IndexError:
                delta = peaks[i].BGSScore

            deltaSn = delta/abs(peaks[i].BGSScore)
            deltaScores.append((delta, deltaSn))

        return peaks[:self._topX], deltaScores[:self._topX]

    def IfIsPeak(self, startRTIndex, endRTIndex, rawScore, rawScoreMtx, pepNorm, fast=True):
        """ This function determines whether the given peak passes empirical criteria """
        peakIonMatch = 0
        massErrMean = 0.0
        massErrVar = 0.0
        massErrors = []
        cIonsMtx = [rawScoreMtx]
        cIonsScores = []
        if rawScore == 0.0:
            isPeak = False
        else:
            scoreThreshold = rawScore / (self._numComp**self._alpha)
            isPeak = False
            for mz in self._peptide.GetFragmentIonMz():
                ionMtx = self._ms2.GetMzScoreMtx(mz)[startRTIndex:endRTIndex]/pepNorm
                ionScore = sum(ionMtx)
                if ionScore >= scoreThreshold:
                    peakIonMatch += 1
                    cIonsScores.append((mz, ionScore/(endRTIndex-startRTIndex)))
                    cIonsMtx.append(ionMtx)
                    if not isPeak and peakIonMatch > self._minIonMatch:
                        isPeak = True
                        if fast:
                            break

                    if not fast:
                        ''' Calculate massErr per transition so that every "matched" ion will contribute a weightedMassErr '''
                        intensities = self._ms2.GetMzScoreMtx(mz)[startRTIndex:endRTIndex]/self._ms2.GetMzWeight(mz)
                        if sum(intensities) == 0:
                            continue
                        weightedMassErr = sum(np.multiply(self._ms2.GetMassErrMtx(mz)[startRTIndex:endRTIndex], intensities)/sum(intensities))
                        massErrors.append(weightedMassErr)

            if isPeak and not fast:
                massErrMean = np.mean(massErrors)
                massErrVar = np.var(massErrors)
                ''' Calculate ion score correlation coefficient with the total score '''
                coef = np.corrcoef(cIonsMtx, rowvar=1)[0]
                cIonsScores = [cIonsScores[i-1] for i in xrange(1, len(cIonsMtx)) if coef[i] > 0.5]

        return isPeak, peakIonMatch, massErrMean, massErrVar, cIonsScores


class RetentionTimeCalibrator():
    def __init__(self, tList, dList):
        self._rankTList = sorted(tList, key=operator.attrgetter('deltaSn'), reverse=True)
        self._rankDList = sorted(dList, key=operator.attrgetter('deltaSn'), reverse=True)
        self._m = None
        self._c = None

    def Calibrate(self, calibrationSize=None):
        if calibrationSize is None:
            targetNumber = len(self._rankTList)
            calibrationSize = 0
            dIndex = 0
            for tIndex in xrange(0, targetNumber, 99):
                if self._rankTList[tIndex].deltaSn >= self._rankDList[dIndex].deltaSn:
                    dIndex +=1
                    calibrationSize = tIndex +1
                else: 
                    break 
            
            if calibrationSize <3:
                calibrationSize = 3 
            
        logger.info('SSR calibration size = %s', calibrationSize)
        
        hydrophobicities = []
        bestPeakTimes = []
        for rank in xrange(0, calibrationSize):
            hydrophobicities.append(self._rankTList[rank].peptide.GetSSRIndex())
            bestPeakTimes.append(self._rankTList[rank].pepFeature.GetTime())

        x = np.array(hydrophobicities)
        y = np.array(bestPeakTimes)
        A = np.vstack([x, np.ones(len(x))]).T
        m, c = np.linalg.lstsq(A, y)[0]
        logger.info('SSR calibration m=%f\tc=%f', m, c)
        
        for result in self._rankTList + self._rankDList:
            timeSSR = m*result.peptide.GetSSRIndex() + c 
            #result.pepFeature.SetSSRTime(timeSSR)

#!/usr/bin/env python

"""
%
% Original author: Ying Sonia Ting <sonia810 at uw.edu>
%                  MacCoss Lab, Department of Genome Sciences, University of Washington
%
% Date: 10/22/2013
%
% These classes handle basic sequence-based peptide properties 
% The class 'MassDistribution' generates peptide isotope distribution using method reported by Kubinyi, Analytica Chimica Acta, 247 (1991) 107-119.
% Python code adapted from Skyline "MassDistribution.cs", "IsotopeAbundances.cs", "AminoAcidFormula.cs" and "IsotopeDistInfo.cs".
%    
%
"""

import sys
import re
import random
import string
from subprocess import Popen, PIPE
from .PecanUtil.peptideCalculator import get_ion_masses
from .PecanUtil.decoyGenerator import pep_shuffle

#########################################################################
# Set default values

PRECISION = 0.000001   # 1 ppm
RESOLUTION = 0.001
MIN_ABUNDANCE = 0.00001 

decoyTag = "decoy"

mono_masses = dict()
mono_masses['H'] = 1.007825035
mono_masses['C'] = 12.00000
mono_masses['O'] = 15.99491463
mono_masses['N'] = 14.003074
mono_masses['I'] = 126.904473
mono_masses['S'] = 31.9720707
mono_masses['P'] = 30.973762
# heavy 13C and 15N
mono_masses['13C'] = 13.0033554
mono_masses['15N'] = 15.0001088

hydrogen_mass = 1.007825035
proton_mass = 1.00727646677
OH_mass = mono_masses['O'] + mono_masses['H']
H2O_mass = mono_masses['O'] + 2*(mono_masses['H'])
NH3_mass = mono_masses['N'] + 3*(mono_masses['H'])
CO_mass = mono_masses['C'] + mono_masses['O']
cam_mass = 2*mono_masses['C'] + 3*mono_masses['H'] + mono_masses['O'] + mono_masses['N'] #iodoacetamide reacts with cysteine to form S-carbamidomethyl cysteine

'''
mono_residue_masses = dict()
mono_residue_masses['A'] = 71.037114
mono_residue_masses['R'] = 156.101111
mono_residue_masses['N'] = 114.042927
mono_residue_masses['D'] = 115.026943
mono_residue_masses['E'] = 129.042593
mono_residue_masses['Q'] = 128.058578
mono_residue_masses['G'] = 57.021464
mono_residue_masses['H'] = 137.058912
mono_residue_masses['I'] = 113.084064
mono_residue_masses['L'] = 113.084064
mono_residue_masses['K'] = 128.094963
mono_residue_masses['M'] = 131.040485
mono_residue_masses['F'] = 147.068414
mono_residue_masses['P'] = 97.052764
mono_residue_masses['S'] = 87.032028
mono_residue_masses['T'] = 101.047679
mono_residue_masses['U'] = 150.95363
mono_residue_masses['W'] = 186.079313
mono_residue_masses['Y'] = 163.06332
mono_residue_masses['V'] = 99.068414
mono_residue_masses['X'] = 113.084064 # L or I
mono_residue_masses['Z'] = 999999999.999

## heavy labeled c-terminal R, K
mono_residue_masses['B'] = mono_residue_masses['R']+ 10.008269 # heavy labeled R
mono_residue_masses['J'] = mono_residue_masses['K']+ 8.014199  # heavy labeled K
'''

residue_composition = dict()                                           # all residue compositions are -H2O
residue_composition['A'] = {'H': 5,  'C': 3,  'O': 1, 'N': 1}
# residue_composition['C'] = {'H': 8,  'C': 5,  'O': 2, 'N': 2, 'S': 1}  ## with cam iodoacetamide
residue_composition['C'] = {'H': 5,  'C': 3,  'O': 1, 'N': 1, 'S': 1}  # no modification!
residue_composition['D'] = {'H': 5,  'C': 4,  'O': 3, 'N': 1}
residue_composition['E'] = {'H': 7,  'C': 5,  'O': 3, 'N': 1}
residue_composition['F'] = {'H': 9,  'C': 9,  'O': 1, 'N': 1}
residue_composition['G'] = {'H': 3,  'C': 2,  'O': 1, 'N': 1}
residue_composition['H'] = {'H': 7,  'C': 6,  'O': 1, 'N': 3}
residue_composition['I'] = {'H': 11, 'C': 6,  'O': 1, 'N': 1}
residue_composition['K'] = {'H': 12, 'C': 6,  'O': 1, 'N': 2}
residue_composition['L'] = {'H': 11, 'C': 6,  'O': 1, 'N': 1}
residue_composition['M'] = {'H': 9,  'C': 5,  'O': 1, 'N': 1, 'S': 1}
residue_composition['N'] = {'H': 6,  'C': 4,  'O': 2, 'N': 2}
residue_composition['P'] = {'H': 7,  'C': 5,  'O': 1, 'N': 1}
residue_composition['Q'] = {'H': 8,  'C': 5,  'O': 2, 'N': 2}
residue_composition['R'] = {'H': 12, 'C': 6,  'O': 1, 'N': 4}
residue_composition['S'] = {'H': 5,  'C': 3,  'O': 2, 'N': 1}
residue_composition['T'] = {'H': 7,  'C': 4,  'O': 2, 'N': 1}
residue_composition['U'] = {'H': 5,  'C': 3,  'O': 1, 'N': 1}   # and 'Se':1
residue_composition['V'] = {'H': 9,  'C': 5,  'O': 1, 'N': 1}
residue_composition['W'] = {'H': 10, 'C': 11, 'O': 1, 'N': 2}
residue_composition['Y'] = {'H': 9,  'C': 9,  'O': 2, 'N': 1}

residue_composition['X'] = {'H': 11, 'C': 6,  'O': 1, 'N': 1}   # L or I
residue_composition['Z'] = {'H':999, 'C':999, 'O':999,'N':999}  # E or Q, differ by 1 Da, always ignored
# heavy labeled c-terminal R, K
residue_composition['B'] = {'H': 12, '13C': 6,  'O': 1, '15N': 4}
residue_composition['J'] = {'H': 12, '13C': 6,  'O': 1, '15N': 2}


# PTM composition for isotopeDist # TODO
ptm_composition = dict()
ptm_composition['UNIMOD:01'] = {'H': 2,  'C': 2,  'O': 1,  'N': 0}
ptm_composition['UNIMOD:02'] = {'H': 1,  'C': 0,  'O': -1, 'N': 1}
ptm_composition['UNIMOD:03'] = {'H': 14, 'C': 10, 'O': 2,  'N': 2, 'S': 1}
ptm_composition['UNIMOD:04'] = {'H': 3,  'C': 2,  'O': 1,  'N': 1}
ptm_composition['UNIMOD:05'] = {'H': 1,  'C': 1,  'O': 1,  'N': 1}
ptm_composition['UNIMOD:06'] = {'H': 2,  'C': 2,  'O': 2,  'N': 0}
ptm_composition['UNIMOD:07'] = {'H': -1, 'C': 0,  'O': 1,  'N': -1}
ptm_composition['UNIMOD:21'] = {'H': 1,  'C': 0,  'O': 3,  'N': 0, 'P': 1}
ptm_composition['UNIMOD:34'] = {'H': 2,  'C': 1,  'O': 0,  'N': 0}


# including heavy 13C and 15N
peptideElements = ['H', 'C', 'O', 'N', 'S', '13C', '15N']

element_isotope = dict()
element_isotope['H'] = { 1.0078246: 0.999855,        2.0141021: 0.000145 }
element_isotope['C'] = { 12.000000: 0.98916,        13.0033554: 0.01084 }
element_isotope['O'] = {15.9949141: 0.997576009706, 16.9991322: 0.000378998479, 17.9991616: 0.002044991815}
element_isotope['N'] = {14.0030732: 0.99633,        15.0001088: 0.00366 }
element_isotope['S'] = { 31.972070: 0.95021,         32.971456: 0.00745,         33.967866: 0.04221,  35.96708: 0.00013}
element_isotope['P'] = {}
# heavy 13C and 15N
element_isotope['13C'] = {  13.0033554: 1.000 }
element_isotope['15N'] = {  15.0001088: 1.000 }


def genModSequence(sequence, diffMasses=None):
    # return a sequence with the form of PEPTID[+100.11]ER
    if diffMasses is None:
        return sequence
    else:
        assert(len(sequence)==len(diffMasses))
        modSeq = sequence
        for index in range(len(diffMasses)-1, -1, -1):
            if diffMasses[index] is not None:
                modSeq = modSeq[:index+1]+diffMasses[index].tag+modSeq[index+1:]

        return modSeq


class DiffMass(object):
    """ A class containing information about a differential mass only modification (not including mod composition)"""
    def __init__(self, monoDiffMass=0.0, avgDiffMass=0.0):
        self._monoMass = monoDiffMass
        self._avgMass = avgDiffMass
        if monoDiffMass >= 0:
            self._tag = '[+{s}]'.format(s=monoDiffMass)
            self._formatedTag = '[+{:.2f}]'.format(monoDiffMass)
        else:
            self._tag = '[{s}]'.format(s=monoDiffMass)
            self._formatedTag = '[{:.2f}]'.format(monoDiffMass)

    @property
    def monoMass(self):
        return self._monoMass

    @property
    def avgMass(self):
        return self._avgMass

    @property
    def tag(self):
        return self._tag

    @property
    def formatedTag(self):
        return self._formatedTag


class BasicPeptide(object):
    def __init__(self, sequence, charge, proteinNames=list()):
        sequence = string.translate(sequence, None, deletions='#*')
        self._ID = None
        self._charge = charge
        self._proteinNames = proteinNames
        monoMass = H2O_mass
        composition = {}.fromkeys(peptideElements, 0)
        composition['H'] += 2
        composition['O'] += 1

        self._modSequence = sequence
        self._mzShift = 0.0

        # check for modifications
        if string.find(sequence, '[') == -1:
            self._sequence = sequence
            self._diffMasses = None
        else:
            mod = re.compile('\[[+,-]\d+\.?\d*\]')
            self._sequence = re.sub(mod, '', sequence) # no mod sequence
            self._diffMasses = [None]*len(self._sequence)
            sumModLength = 0
            for m in mod.finditer(sequence):
                aaIndex = m.start()-sumModLength-1
                sumModLength += m.end()-m.start()
                mass = float(sequence[m.start()+1:m.end()-1])
                monoMass += mass
                self._mzShift += mass
                self._diffMasses[aaIndex] = DiffMass(mass, mass)
            self._mzShift /= self._charge

        for aa in self._sequence:
                for element in residue_composition[aa]:
                    monoMass += residue_composition[aa][element]*mono_masses[element]
                    composition[element] += residue_composition[aa][element]

        self._composition = composition
        self._monoMass = monoMass
        self._precursorMz = (monoMass+(self._charge*proton_mass))/self._charge

    def AddAProteinName(self, name):
        self._proteinNames.append(name)

    def GetProteinNames(self):
        if self._proteinNames is None:
            return None
        else:
            return tuple(self._proteinNames)

    def GetProteinID(self):
        return ",".join(self._proteinNames)

    def GetSequence(self):
        return self._sequence

    def GetModSequence(self):
        return self._modSequence

    def GetDiffMasses(self):
        if self._diffMasses is None:
            return None
        else:
            return tuple(self._diffMasses)

    def GetCharge(self):
        return self._charge

    def GetMass(self):
        return self._monoMass

    def GetPrecursorMz(self):
        return self._precursorMz

    def GetComposition(self):
        return self._composition
        
    def GetLength(self):
        return len(self._sequence)


class Peptide(BasicPeptide):
    def __init__(self, sequence, charge, tag=1, proteinNames=None):
        BasicPeptide.__init__(self, sequence, charge, proteinNames)
        
        ''' calculate when first asked (lazy evaluation)'''
        self._isotopeDist = None
        self._spectrumMz = None
        self._ionSeries = None
        self._hydrophobicity = None

        if tag == 1:
            self._tag = tag
        elif tag == -1:
            self._tag = tag
            #self._ID = "%s%s" % (decoyTag, self.GetID())
        else:
            sys.stderr.write("peptide tag should be 1 (target) or -1 (decoy)\n")
            sys.exit(1)

    def GetID(self):
        if self._ID is None:
            if self._diffMasses is None:
                self._ID = "%s+%s" % (self._sequence, self._charge)
            else:
                ID = self._sequence
                for index in xrange( len(self._diffMasses)-1, -1, -1):
                    if self._diffMasses[index] is not None:
                        tag = self._diffMasses[index].formatedTag
                        ID = ID[:index+1] + tag + ID[index+1:]
                self._ID = "%s+%s" % (ID, self._charge)
            if self._tag == -1:
                self._ID = "%s%s" % (decoyTag, self._ID)
        return self._ID

    def GetTag(self):
        return self._tag

    def GetIsotopeDist(self):
        if self._isotopeDist is None:
            noModPrecursorMz = self._precursorMz - self._mzShift
            md = MassDistribution()
            m = md
            for element in self._composition.keys():
                m = m.Add(md.Add(element_isotope[element]).Multiply(self._composition[element]))
            self._isotopeDist = m.GetMzDistribution(self._charge, noModPrecursorMz)
            if self._mzShift != 0.0:
                for mz in self._isotopeDist.keys():
                    self._isotopeDist[mz+self._mzShift] = self._isotopeDist.pop(mz)
        return self._isotopeDist
    
    def GetFragmentIonMz(self, mzBoundaries=None, ionTypes=('b','y')):
        if self._spectrumMz is None:
            fg = FragmentGenerator(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
            self._spectrumMz, self._ionSeries = fg.GetFragmentation(self._sequence, self._charge, self._diffMasses)
            self._spectrumMz = tuple(self._spectrumMz)
            self._ionSeries = tuple(self._ionSeries)
        return self._spectrumMz

    def GetIonSeries(self):
        if self._ionSeries is None:
            sys.stderr.warning('Call GetFragmentIonMz before GetIonSeries')
            sys.exit()
        return self._ionSeries

    def GetSSRIndex(self):
        if self._hydrophobicity is None:
            # fixMe
            ssrLoc = ''
            querySeq = self._sequence
            p = Popen([ssrLoc, '--alg', '3.0', '--seq', querySeq, '--output', 'tsv', '--A', '0', '--B', '0'],
                      stdout=PIPE,
                      close_fds=True)
            ssrOut = p.stdout.readlines()
            p.stdout.close()
            self._hydrophobicity = float(ssrOut[0].split('\t')[2])
        
        return self._hydrophobicity


class FragmentGenerator(object):
    def __init__(self, mzBoundaries=None, ionTypes=('b', 'y')):
        self._mzBoundaries = mzBoundaries
        self._ionTypes = ionTypes

    def GetFragmentation(self, sequence, charge, diffMasses=None):
        spectrum = []       # mz of b/y-ions
        ionSeries = []      # type of ion at each mz
        for ionType in self._ionTypes:
            ion_mzs = get_ion_masses(sequence, 1, ionType, diffMods=diffMasses)
            if charge >= 3:
                ion_mzs += get_ion_masses(sequence, 2, ionType, diffMods=diffMasses)
            
            if self._mzBoundaries is None:
                spectrum += [round(mz, 5) for mz in ion_mzs]
                ionSeries += [ionType]*len(ion_mzs)
            else:
                filtered_mzs = [round(mz, 5) for mz in ion_mzs if self._mzBoundaries[0] <= mz < self._mzBoundaries[-1]]
                spectrum += filtered_mzs
                ionSeries += [ionType]*len(filtered_mzs)
        
        assert len(spectrum) == len(ionSeries)

        return spectrum, ionSeries


class DecoyGenerator(object):
    def __init__(self, proteomePeptideList, mzBoundaries=None, ionTypes=('b', 'y')):
        self._proteomePeptideList = proteomePeptideList
        self._mzBoundaries = mzBoundaries
        self._fg = FragmentGenerator(mzBoundaries=mzBoundaries, ionTypes=ionTypes)
        self._decoyTag = decoyTag

    def GetSmartDecoy(self, refPeptide):
        # create decoy sequence by shuffling and reversing
        refSequence = refPeptide.GetSequence()
        refSpectrum = refPeptide.GetFragmentIonMz()
        decoyProteinNames = []
        if refPeptide.GetProteinNames() is None:
            decoyProteinNames.append("DECOY")
        else:
            for proteinName in refPeptide.GetProteinNames():
                decoyProteinNames.append("DECOY_%s" % proteinName)

        attempts = 0
        maxTry = 10
        minShared = 1.0
        sharedIonsPercentage = 1.0
        bestDecoy = ""
        # seeding for random number generator used for generating seeds
        random.seed(hash(refSequence))
        seeds = [random.random()*x for x in range(maxTry)]

        decoySequence, decoyDiffMasses = pep_shuffle(refSequence, refPeptide.GetDiffMasses(), seed=seeds[attempts])
        decoySpectrum = self._fg.GetFragmentation(decoySequence, refPeptide.GetCharge(), decoyDiffMasses)[0]
        while attempts < maxTry:
            if decoySequence in self._proteomePeptideList:
                decoySequence, decoyDiffMasses = pep_shuffle(decoySequence, decoyDiffMasses, seed=seeds[attempts])
                decoySpectrum = self._fg.GetFragmentation(decoySequence, refPeptide.GetCharge(), decoyDiffMasses)[0]
            else:
                sharedIonsPercentage = self.CalcSharedIon(refSpectrum, decoySpectrum)
                if sharedIonsPercentage > 0.4:
                    if sharedIonsPercentage < minShared:
                        minShared = sharedIonsPercentage
                        bestDecoy = decoySequence
                    decoySequence, decoyDiffMasses = pep_shuffle(decoySequence, decoyDiffMasses, seed=seeds[attempts])
                    decoySpectrum = self._fg.GetFragmentation(decoySequence, refPeptide.GetCharge(), decoyDiffMasses)[0]
                else:
                    break
            attempts += 1

        if sharedIonsPercentage > minShared:
            decoySequence = bestDecoy

        return Peptide(genModSequence(decoySequence, decoyDiffMasses), refPeptide.GetCharge(),
                       tag=-1, proteinNames=decoyProteinNames)

    def GetDecoy(self, refPeptide):
        # create decoy sequence by shuffling and reversing
        refSequence = refPeptide.GetSequence()
        attempts = 0
        maxTry = 3
        # seeding for random number generator used for generating seeds
        random.seed(hash(refSequence))
        seeds = [random.random()+x for x in range(maxTry)]

        decoySequence, decoyDiffMasses = pep_shuffle(refSequence, refPeptide.GetDiffMasses(), seed=seeds[attempts])
        while attempts < maxTry:
            if decoySequence in self._proteomePeptideList:
                decoySequence, decoyDiffMasses = pep_shuffle(decoySequence, decoyDiffMasses, seed=seeds[attempts])
            else:
                break
            attempts += 1

        if decoySequence in self._proteomePeptideList:
            decoySequence, decoyDiffMasses = pep_shuffle(decoySequence, decoyDiffMasses, seed=attempts)

        return Peptide(genModSequence(decoySequence, decoyDiffMasses), refPeptide.GetCharge(), tag=-1)

    def CalcSharedIon(self, refSpectrum, decoySpectrum):
        totalIons = len(decoySpectrum)
        overlap = 0.0
        for mz in decoySpectrum:
            if mz in refSpectrum:
                overlap += 1
        sharedIonsPercentage = overlap/totalIons
        return sharedIonsPercentage


class MassDistribution(dict):
    def __init__(self, *args):
        dict.__init__(self)
        if len(args):
            self.update(*args) 
        else:
            self[0.0] = 1.0
        self._resolution = RESOLUTION 
        self._minAbundance = MIN_ABUNDANCE

    def Add(self, addition):
        map = {}
        for mass1 in self.keys():
            for mass2 in addition.keys():
                mass = mass1 + mass2
                if mass in map.keys():
                    frequency = map[mass]
                else:
                    frequency = 0.0
                frequency += self[mass1] * addition[mass2]
                map[mass] = frequency
        
        newMap = MassDistribution(map).NewInstance()
        return newMap

    def Multiply(self, factor):
        if factor == 0:
            return MassDistribution()
        if factor == 1:
            return self

        result = MassDistribution.Add(self, self)
        if factor >= 4:
            result = result.Multiply(int(factor/2))
        if (factor % 2) != 0:
            result = result.Add(self)

        return result

    def GetMzDistribution(self, charge, precursorMz):
        mz_map = {}
        mzMergingRange = hydrogen_mass / (charge*2)
        for mass in sorted(self.keys()):
            mz = proton_mass + (mass/charge)
            mz_map[mz] = self[mass]

        m = MassDistribution(mz_map)
        m._resolution = mzMergingRange
        m = m.NewInstance()
        mzs = sorted(m.keys())
        mz_map = {}
        maxFrequency = max(m.values())
        
        '''Return only M-1, M, M+1, M+2, M+3 '''
        mz_map[(mzs[0] - (hydrogen_mass / charge))] = 0.0
        for i in xrange(0, 4):
            mz = mzs[i]
            mz_map[mz] = m[mz] / maxFrequency
            if i == 0:
                assert abs(mz-precursorMz) < self._resolution
        
        return mz_map
        # return MassDistribution(mz_map)

    def NewInstance(self):
        ''' Filter masses by resolution and abundance '''
        map = {}
        curMass = 0.0
        curFrequency = 0.0
        totalAbundance = 0.0
        
        for mass in sorted( self.keys()):
            frequency = self[mass]
            if frequency == 0:
                continue
            
            if ( mass - curMass) > self._resolution:
                ''' mass is a new peak. store the curMass '''
                if curFrequency > self._minAbundance:
                    map[curMass] = curFrequency
                    totalAbundance += curFrequency
                curMass = mass
                curFrequency = frequency
            else:
                ''' mass inseparatable from curMass. adjust the center of curMass (weighted average) and add frequency '''
                curMass = ((curMass*curFrequency)+(mass*frequency))/(curFrequency + frequency)
                curFrequency += frequency
       
        ''' include the last curMass '''
        if curFrequency > self._minAbundance:
            map[curMass] = curFrequency
            totalAbundance += curFrequency 

        ''' recalculate the proportions '''
        if totalAbundance != 1.0:
            for mass in map.keys():
                map[mass] = map[mass]/totalAbundance

        return MassDistribution(map)


#!/usr/bin/env python 
import sys
import gc
import logging
import numpy as np
import collections
import pymzml
from subprocess import Popen, PIPE
from .TheoreticalGenerator import MZExtracter

logger = logging.getLogger('PECAN.Chromatogram')


class Chromatogram(object):
    def __init__(self, mz=0, intensities=None, size=6000, tType=None):
        self._mz = mz
        self._tType = tType   # transition type: i or e (used in background estimation only)

        if intensities is None:
            self._intensities = np.zeros(size)
            if self._tType == 'e':
                self._massErrors = None
            else: 
                self._massErrors = np.zeros(size, dtype=np.int)
        else:
            self._intensities = np.array(intensities)
            if self._tType == 'e':
                self._massErrors = None
            else:
                self._massErrors = np.zeros(size, dtype=np.int)

    def GetMz(self):
        return self._mz

    def AddValue(self, mz=0, intensity=0, rtIndex=0):
        self._intensities[rtIndex] += intensity
        
        if self._tType != 'e':
            massErr = int((mz-self._mz)/self._mz * 10**6)
            if self._massErrors[rtIndex] == 0:
                self._massErrors[rtIndex] = massErr
            elif self._massErrors[rtIndex] > massErr:  # use the smallest massErr within the mz tolerance
                self._massErrors[rtIndex] = massErr

    def GetMtx(self):
        return self._intensities

    def GetMassErrMtx(self):
        return self._massErrors


class MS2Data(object):
    def __init__(self, tolerance=10, unit='ppm', chromSize=0):
        self._rTimes = []
        self._norms = []
        self._data = {} 
        self._emz = []  # e type mz used for background estimation only
        self._chromSize = chromSize
        self._cycleTime = 0
        self._tolerance = tolerance
        self._unit = unit
        self._scanNr = []

    def GetMassErr(self, mz):
        if self._unit == 'mz':
            return self._tolerance
        elif self._unit == 'ppm':
            return mz*self._tolerance*(10**-6)
        else:
            logger.warning('Unknown unit %s\nUse ppm\n', self._unit)
            return mz*self._tolerance*(10**-6)

    def CheckChromSize(self, mzmlFileName, minPrecursor=200.0, maxPrecursor=2000.0, SCIEX=None):
        if self._chromSize > 0:
            logger.info('Chromatogram size already known: %s', self._chromSize)
            pass
        else:
            chromSize = 0
            cycleTime = 0.0
            run = pymzml.run.Reader(mzmlFileName)

            if SCIEX:
                for spectrum in run:
                    if spectrum['id'] > 1:
                        precursorMz = float(spectrum['MS:1000744'])
                        if minPrecursor <= precursorMz < maxPrecursor:
                            chromSize += 1
                            if chromSize == 100:
                                cycleTime = float(spectrum['MS:1000016'])*60/100
            else:
                for spectrum in run:
                    if spectrum['ms level'] == 2:
                        precursorMz = float(spectrum['MS:1000744'])
                        if minPrecursor <= precursorMz < maxPrecursor:
                            chromSize += 1
                            if chromSize == 100:
                                cycleTime = float(spectrum['MS:1000016'])*60/100
        
            logger.info('Scanned %s MS2 scans with precursor m/z %s - %s', chromSize, minPrecursor, maxPrecursor)
            logger.info('Cycle time for the first 100 cycles: %.6f seconds', cycleTime)
            if chromSize == 0:
                logger.info('%d MS2 scans! Quit now', chromSize)
                sys.exit()
            elif cycleTime == 0:
                logger.info('Failed to calculate cycle time!')
                if chromSize < 100:
                    logger.info('Contain less than 100 scans! Irregular DIA. Assume very long cycle time')
                    self._cycleTime = 1000
                    self._chromSize = chromSize
                else:
                    logger.info('Contain more than 100 scans! Irregular DIA. Assume very short cycle time')
                    self._cycleTime = 0.001
                    self._chromSize = chromSize
            else:
                self._chromSize = chromSize
                self._cycleTime = cycleTime

    def LoadFromMZML(self, mzmlFileName, fragmentIndex,
                     minPrecursor=200.0, maxPrecursor=2000.0, SCIEX=None):

        ChromInfo = collections.namedtuple('ChromInfo', 'mtx massErrMtx weight probability')
        logger.info('====== LOADING MS2: %s ======', mzmlFileName)
        logger.info('Extracting %s chromatograms',  len(fragmentIndex.GetList()))
        
        # initialize Chromatogram instances for mz needed
        chromInstances = []
        lowerMzs = []
        upperMzs = []

        for mz in fragmentIndex.GetList():
            ch = Chromatogram(mz=mz, size=self._chromSize, tType=fragmentIndex.GetMzType(mz))
            if fragmentIndex.GetMzType(mz) == 'e':
                self._emz.append(mz)

            chromInstances.append(ch)
            massErr = self.GetMassErr(mz)
            lowerMzs.append(mz - massErr)
            upperMzs.append(mz + massErr)
        
        rtIndex = 0             # # of scan/RT with target precursor m/z
        mzExtract = MZExtracter(chromInstances, lowerMzs, upperMzs )
        
        run = pymzml.run.Reader(mzmlFileName)
        if SCIEX:
            for spectrum in run:
                if spectrum['id'] != 1:
                    precursorMz = float(spectrum['MS:1000744'])
                    if minPrecursor <= precursorMz < maxPrecursor:
                        rTime = float(spectrum['MS:1000016'])
                        self._rTimes.append(rTime)
                        self._scanNr.append(int((rTime/90.0)*50000))
                        logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                        if spectrum.i is None:  # empty scan
                            self._norms.append(0.0)
                            pass
                        else:
                            intensities = np.sqrt(spectrum.i)
                            scan = zip(spectrum.mz, intensities)
                            self._norms.append(np.linalg.norm(intensities))
                            mzExtract.Extract(scan, rtIndex=rtIndex)
                        rtIndex += 1
        else:
            for spectrum in run:
                if spectrum['ms level'] == 2:
                    precursorMz = float(spectrum['MS:1000744'])
                    if minPrecursor <= precursorMz < maxPrecursor:
                        self._rTimes.append(float(spectrum['MS:1000016']))
                        self._scanNr.append(spectrum['id'])
                        logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                        if spectrum.i is None:  # empty scan
                            self._norms.append(0.0)
                            pass
                        else:
                            intensities = np.sqrt(spectrum.i)
                            scan = zip(spectrum.mz, intensities)
                            self._norms.append(np.linalg.norm(intensities))
                            mzExtract.Extract(scan, rtIndex=rtIndex)
                        rtIndex += 1

        logger.info('Loaded %s MS2 scans with precursor m/z %s - %s', len(self._rTimes), minPrecursor, maxPrecursor)

        for ch in chromInstances:
            mz = ch.GetMz()
            weight = fragmentIndex.GetMzWeight(mz)
            self._data[mz] = ChromInfo(mtx=ch.GetMtx()*weight, massErrMtx=ch.GetMassErrMtx(), weight=weight,
                                       probability=fragmentIndex.GetMzProbability(mz))
        
        del fragmentIndex, mzExtract, chromInstances, upperMzs, lowerMzs, minPrecursor, maxPrecursor
        gc.collect()

    def LoadFromMS2(self, ms2FileName, mzBoundaries, fragmentIndex,
                    minPrecursor=200.0, maxPrecursor=2000.0, ppm=10):
        rtIndex = 0             # # of scan/RT with target precursor m/z
        scan = []               # per scan event [(mz, intensity), (mz, intensity), ...]
        scanIntensities = {}    # per scan event key=mz, value=intensity
        inWindow = 0

        ChromInfo = collections.namedtuple('ChromInfo', 'mtx massErrMtx weight probability')
        totalTrans = len(fragmentIndex.GetList())
        
        p = Popen(['grep', '^S', ms2FileName], stdout=PIPE, close_fds=True)
        chromSize = 0 
        for sline in p.stdout.readlines():
            precursorMz = float(sline.split()[3])
            if minPrecursor <= precursorMz < maxPrecursor:
                chromSize += 1

        logger.info('Scanned %s MS2 scans with precursor m/z %s - %s', chromSize, minPrecursor, maxPrecursor)        
        if chromSize == 0:
            logger.info('%d MS2 scans! Quit now', chromSize)
            sys.exit()
        
        logger.info('====== LOADING MS2: %s ======', ms2FileName)
        logger.info('Extracting %s chromatograms', totalTrans)

        # initialize Chromatogram instances for mz needed
        chromInstances = []
        lowerMzs = []
        upperMzs = []
        
        for mz in fragmentIndex.GetList():
            ch = Chromatogram(mz=mz, size=chromSize, tType=fragmentIndex.GetMzType(mz)) 
            chromInstances.append(ch)
            massErr = mz * ppm * (10**-6)
            lowerMzs.append(mz - massErr)
            upperMzs.append(mz + massErr)
        
        ms2File = open(ms2FileName, "r")
        mzExtract = MZExtracter(chromInstances, lowerMzs, upperMzs )
        skipHeaders = ['H', 'Z', 'D']
        for line in ms2File:
            words = line.split()
            if words[0] in skipHeaders:
                continue
            elif words[0] is 'S':
                # process the previous scan event
                # currently will always leave out the last scan #### FIX ME!
                if inWindow and scan:
                    
                    self._rTimes.append(retentionTime)
                    self._norms.append(np.linalg.norm( scanIntensities))

                    logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                    mzExtract.Extract(scan, rtIndex=rtIndex)
                    rtIndex += 1
                 
                scan = []
                scanIntensities = []
                # process the current scan event
                precursorMz = round(float(words[3]), 2)
                if minPrecursor <= precursorMz < maxPrecursor:
                    inWindow = 1
                else:
                    inWindow = 0

            elif inWindow:
                if words[1] == "RTime":
                    retentionTime = float(words[2])
                elif len(words) == 2 and words[1] != 0:
                    mz = float(words[0])
                    if mzBoundaries[0] <= mz < mzBoundaries[-1]:
                        intensity = float(words[1])**0.5
                        scan.append((mz, intensity))
                        scanIntensities.append(intensity)
        
        # fixing the last line issue
        if inWindow and scan:
            self._rTimes.append(retentionTime)
            self._norms.append(np.linalg.norm(scanIntensities))
            logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
            mzExtract.Extract(scan, rtIndex=rtIndex)
            rtIndex += 1

        ms2File.close()
        scanNumber = len(self._rTimes) 
        logger.info('Loaded %s MS2 scans with precursor m/z %s - %s', scanNumber, minPrecursor, maxPrecursor)

        for ch in chromInstances:
            mz = ch.GetMz()
            weight = fragmentIndex.GetMzWeight(mz)
            self._data[mz] = ChromInfo(mtx=ch.GetMtx()*weight, massErrMtx=ch.GetMassErrMtx(), weight=weight,
                                       probability=fragmentIndex.GetMzProbability(mz))

        del fragmentIndex, mzExtract, chromInstances, upperMzs, lowerMzs, scanNumber, minPrecursor, \
            maxPrecursor, scan, scanIntensities
        gc.collect() 
   
    def CleanUp(self):
        for mz in self._emz:
            self._data.pop(mz, None)
        gc.collect()

    def GetMzWeight(self, mz):
        return self._data[mz].weight

    def GetMzProbability(self, mz):
        return self._data[mz].probability

    def GetMzScoreMtx(self, mz):
        return self._data[mz].mtx

    def GetMassErrMtx(self, mz):
        return self._data[mz].massErrMtx
    
    def GetRetentionTimes(self):
        return self._rTimes

    def GetChromatogramSize(self):
        return self._chromSize

    def GetNorms(self):
        return self._norms
   
    def GetDataSize(self):
        return len(self._data.keys())

    def GetCycleTime(self):
        return self._cycleTime

    def GetScanNumber(self, index):
        return self._scanNr[index]


class MS1Data(object):
    def __init__(self, tolerance=10, unit='ppm'):
        self._rTimes = []
        self._norms = []
        self._mzMtx = {}
        self._massErrMtx = {}
        self._tolerance = tolerance
        self._unit = unit

    def GetMassErr(self, mz):
        if self._unit == 'mz':
            return self._tolerance
        elif self._unit == 'ppm':
            return mz*self._tolerance*(10**-6)
        else:
            logger.warning('Unknown unit %s\nUse ppm\n', self._unit)
            return mz*self._tolerance*(10**-6)

    def LoadFromMZML(self, mzmlFileName, mzList=[], SCIEX=None):
        chromSize = 0
        run = pymzml.run.Reader(mzmlFileName)
        if SCIEX:
            for spectrum in run:
                if spectrum['id'] == 1:
                    chromSize += 1
        else:
            for spectrum in run:
                if spectrum['ms level'] == 1:
                    chromSize += 1
        
        logger.info('Scanned %s MS1 scans', chromSize)
        logger.info('====== LOADING MS1: %s ======', mzmlFileName)
        logger.info('Extracting %s chromatograms', len(mzList))

        # initialize Chromatogram instances for mz needed
        chromInstances = []
        lowerMzs = []
        upperMzs = []
        for mz in mzList:
            ch = Chromatogram(mz=mz, size=chromSize, tType='i')
            chromInstances.append(ch)
            massErr = self.GetMassErr(mz)
            lowerMzs.append(mz - massErr)
            upperMzs.append(mz + massErr)
        
        mzExtract = MZExtracter(chromInstances, lowerMzs, upperMzs)
        run = pymzml.run.Reader(mzmlFileName)
        rtIndex = 0
        if SCIEX:
            for spectrum in run:
                if spectrum['id'] == 1:
                    self._rTimes.append(float(spectrum['MS:1000016']))
                    self._norms.append(spectrum.i)
                    logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                    mzExtract.Extract(spectrum.peaks, rtIndex=rtIndex)
                    rtIndex += 1

        else:
            for spectrum in run:
                if spectrum['ms level'] == 1:
                    self._rTimes.append(float(spectrum['MS:1000016']))
                    self._norms.append(spectrum.i)
                    logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                    mzExtract.Extract(spectrum.peaks, rtIndex=rtIndex)
                    rtIndex += 1

        logger.info('Loaded %s MS1 scans', len(self._rTimes))
        
        for ch in chromInstances:
            mz = ch.GetMz()
            self._mzMtx[mz] = ch.GetMtx()
            self._massErrMtx[mz] = ch.GetMassErrMtx()

        del mzList, chromInstances, mzExtract, upperMzs, lowerMzs
        gc.collect()

    def LoadFromMS1(self, ms1FileName,  mzList=None, ppm=10):
        rtIndex = 0             # # or scan/RT 
        scan = []               # per scan event [(mz, intensity), (mz, intensity), ...]
        scanIntensities = []    # per scan event

        cmd = "grep ^S %s | wc -l" % ms1FileName
        p = Popen(cmd, stdout=PIPE, shell=True)
        chromSize = int(p.communicate()[0]) 

        logger.info('Scanned %s MS1 scans', chromSize)        
        logger.info('====== LOADING MS1: %s ======', ms1FileName)
        logger.info('Extracting %s chromatograms', len(mzList))

        # initialize Chromatogram instances for mz needed
        chromInstances = []
        lowerMzs = []
        upperMzs = []
        for mz in mzList:
            ch = Chromatogram( mz=mz, size=chromSize, tType='i') 
            chromInstances.append( ch )
            massErr = mz * ppm *(10**-6)
            lowerMzs.append(mz - massErr)
            upperMzs.append(mz + massErr)
      
        ms1File = open(ms1FileName, "r")
        mzExtract = MZExtracter(chromInstances, lowerMzs, upperMzs)
        skipHeaders = ['H', 'Z', 'D']
        for line in ms1File:
            words = line.split()
            if words[0] in skipHeaders:
                continue
            elif words[0] == "S":
                # process the previous scan event
                # currently will always leave out the last scan #### FIX ME!
                if scan:
                    self._rTimes.append(retentionTime)
                    self._norms.append(np.linalg.norm( scanIntensities ) )

                    logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
                    mzExtract.Extract(scan, rtIndex=rtIndex)
                    rtIndex += 1
                scan = [] 
                scanIntensities = []

            elif words[0] is "I" and words[1] is "RTime":
                retentionTime = float(words[2])
            elif words[0] is "I" and words[1] is "TIC":
                tic = float(words[2])
            elif len(words) == 2 and words[1] != 0:
                mz = float( words[0])
                intensity = float(words[1])
                scan.append( (mz, intensity ))
                scanIntensities.append(intensity)
        if scan:
            self._rTimes.append(retentionTime)
            self._norms.append(np.linalg.norm( scanIntensities))
            logger.debug('RTIndex: %s RT: %s', rtIndex, self._rTimes[rtIndex])
            mzExtract.Extract(scan, rtIndex=rtIndex)
            rtIndex += 1
        ms1File.close()
        scanNumber = len(self._rTimes)
        logger.info('Loaded %s MS1 scans', scanNumber)
        
        for ch in chromInstances:
            mz = ch.GetMz()
            self._mzMtx[mz] = ch.GetMtx()
            self._massErrMtx[mz] = ch.GetMassErrMtx()

        del mzList, chromInstances, mzExtract, upperMzs, lowerMzs 
        gc.collect()

    def GetRetentionTimes(self):
        return self._rTimes

    def GetNorms(self):
        return self._norms

    def GetMzMtx(self, mz):
        return self._mzMtx[mz]

    def GetMassErrMtx(self, mz):
        return self._massErrMtx[mz]

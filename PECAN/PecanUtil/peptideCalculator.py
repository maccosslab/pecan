### modified from Jarrett's peptide_calculator 
#object to calculate peptide masses, with charge taken into account
#return b ions list, y ion list 
#mass mapper, takes epsilon val, spectrum list, b ions, y ions list

#on initialization bisect b ion, y ion list create list of epsilons linked to peaks

#masses from http://www.matrixscience.com/help/aa_help.html
#element masses from www.unimod.org/masses.html

from copy import deepcopy

mono_masses = dict()
avg_masses = dict()

mono_masses['H'] = 1.007825035
avg_masses['H'] = 1.00794

mono_masses['C'] = 12
avg_masses['C'] = 12.0107

mono_masses['O'] = 15.99491463
avg_masses['O'] = 15.9994

mono_masses['N'] = 14.003074
avg_masses['N'] = 14.0067

mono_masses['I'] = 126.904473
avg_masses['I'] = 126.90447

mono_masses['S'] = 31.9720707
avg_masses['S'] = 32.065

mono_masses['P'] = 30.973762
avg_masses['P'] = 30.973761

hydrogen_mass = 1.007825035
proton_mass = 1.00727646677
OH_mass = mono_masses['O'] + mono_masses['H']
H2O_mass = mono_masses['O'] + 2*(mono_masses['H'])
NH3_mass = mono_masses['N'] + 3*(mono_masses['H'])
CO_mass = mono_masses['C'] + mono_masses['O']
cam_mass = 2*mono_masses['C'] + 3*mono_masses['H'] + mono_masses['O'] + mono_masses['N'] #iodoacetamide reacts with cysteine to form S-carbamidomethyl cysteine

avg_hydrogen_mass = avg_masses['H']
avg_OH_mass = avg_masses['O'] + avg_masses['H']
avg_H2O_mass = avg_masses['O'] + 2*(avg_masses['H'])
avg_NH3_mass = avg_masses['N'] + 3*(avg_masses['H'])
avg_CO_mass = avg_masses['C'] + avg_masses['O']
avg_cam_mass = 2*avg_masses['C'] + 3*avg_masses['H'] + avg_masses['O'] + avg_masses['N'] #iodoacetamide reacts with cysteine to form S-carbamidomethyl cysteine

mono_residue_masses = dict()
mono_residue_masses['A'] = 71.037114
mono_residue_masses['R'] = 156.101111
mono_residue_masses['N'] = 114.042927
mono_residue_masses['D'] = 115.026943
#mono_residue_masses['C'] = 103.009185 + cam_mass
mono_residue_masses['C'] = 103.009185
mono_residue_masses['E'] = 129.042593
mono_residue_masses['Q'] = 128.058578
mono_residue_masses['G'] = 57.021464
mono_residue_masses['H'] = 137.058912
mono_residue_masses['I'] = 113.084064
mono_residue_masses['L'] = 113.084064
mono_residue_masses['K'] = 128.094963
mono_residue_masses['M'] = 131.040485
mono_residue_masses['F'] = 147.068414
mono_residue_masses['P'] = 97.052764
mono_residue_masses['S'] = 87.032028
mono_residue_masses['T'] = 101.047679
mono_residue_masses['U'] = 150.95363
mono_residue_masses['W'] = 186.079313
mono_residue_masses['Y'] = 163.06332
mono_residue_masses['V'] = 99.068414
## heavy labeled c-terminal R, K
mono_residue_masses['B'] = mono_residue_masses['R']+ 10.008269 # heavy labeled R
mono_residue_masses['J'] = mono_residue_masses['K']+ 8.014199  # heavy labeled K
mono_residue_masses['X'] = 113.084064  # I or L


avg_residue_masses = dict()
avg_residue_masses['A'] = 71.0779
avg_residue_masses['R'] = 156.1857
avg_residue_masses['N'] = 114.1026
avg_residue_masses['D'] = 115.0874
#avg_residue_masses['C'] = 103.1429 + avg_cam_mass
avg_residue_masses['C'] = 103.1429
avg_residue_masses['E'] = 129.114
avg_residue_masses['Q'] = 128.1292
avg_residue_masses['G'] = 57.0513
avg_residue_masses['H'] = 137.1393
avg_residue_masses['I'] = 113.1576
avg_residue_masses['L'] = 113.1576
avg_residue_masses['K'] = 128.1723
avg_residue_masses['M'] = 131.1961
avg_residue_masses['F'] = 147.1739
avg_residue_masses['P'] = 97.1152
avg_residue_masses['S'] = 87.0773
avg_residue_masses['T'] = 101.1039
avg_residue_masses['U'] = 150.0379
avg_residue_masses['W'] = 186.2099
avg_residue_masses['Y'] = 163.1733
avg_residue_masses['V'] = 99.1311
## heavy labeled c-terminal R, K
avg_residue_masses['B'] = avg_residue_masses['R'] + 9.928665
avg_residue_masses['J'] = avg_residue_masses['K'] + 7.941847
avg_residue_masses['X'] = 113.1576  # I or L

###################################################################################################
def get_ion_masses( sequence, charge, ion_type, average_masses=False, diffMods=None):
		"""
			diffMods can either be none, or a list of differential modifications the same
			length as sequence, containing None if there is no differential mod, or the diff mod
			if there is a diff mod for amino acid at index X
		"""

		#check to see if the peptide has flanking sequence
		useDiffMods = True
		if sequence.count('.')>0:
			sequence = sequence.split('.')[1]
		if diffMods is None:
			useDiffMods = False
		else:
			if len(diffMods)!=len(sequence):
				print diffMods
				print sequence
				raise Exception("[PeptideCalculator2::get_masses]: length of diffMods must = length of sequence\n")
		
		if average_masses:
			callAttr = 'avgMass'
		else:
			callAttr = 'monoMass'
		masses = []
		if len(sequence)>0:
			sequence = sequence.upper()
			#convert sequence into amino acid masses
			aaSequence =  list()
			if useDiffMods:
				for i, aaAbrev in enumerate(sequence):
					aaSequence.append(AminoAcid(aaAbrev, mono_residue_masses[aaAbrev], avg_residue_masses[aaAbrev], diffMods[i]))
			else:
				for aaAbrev in sequence:
					aaSequence.append(AminoAcid(aaAbrev, mono_residue_masses[aaAbrev], avg_residue_masses[aaAbrev])) 
				
			seq_list = []
			if ion_type[0] in ['b', 'a']:
				for i in range(2,len(aaSequence)):
					seq_list.append(aaSequence[:i])
			elif ion_type[0]=='y':
				for i in range(1,len(aaSequence)-charge+1):
					seq_list.append(aaSequence[i:])

			for frag in seq_list:
				fragM = 0
				for aa in frag:
					fragM += getattr(aa, callAttr)
				if ion_type[0] == 'y':
					if average_masses:
						fragM += avg_OH_mass + avg_hydrogen_mass
					else:
						fragM += OH_mass + hydrogen_mass
				elif ion_type[0] == 'a':
					if average_masses:
						fragM = fragM - avg_CO_mass
					else:
						fragM = fragM - CO_mass
				if len(ion_type)>1 and ion_type[1] == '*':
					if average_masses:
						fragM -= avg_NH3_mass
					else:
						fragM-= NH3_mass
				elif len(ion_type)>1 and ion_type[1] == 'o':
					if average_masses:
						fragM -= avg_H2O_mass
					else:
						fragM -= H2O_mass
				#now frag1M/frag2M is the non-charged mass
				frag_ch_mass = (fragM + (charge*proton_mass))/float(charge)
				masses.append(frag_ch_mass)
		masses.sort()
		return masses



class DiffMod(object):
	'''A class containing information about a differential modification'''
	def __init__(self, modName = None, aminoAcid = None, probability = None, composition = None):
		self._modName = modName	#Name of modification
		self._aminoAcid = aminoAcid	#Amino acid modified
		self._probability = probability	#likelihood of occurrence
		if composition is not None:
			self.composition = composition	#list of atoms making up the modification
		else:
			self._monoMass = 0
			self._avgMass = 0

	##properties##
	@property
	def modName(self):
		return self._modName
	@modName.setter
	def modName(self, value):
		self._modName = value
	
	@property
	def aminoAcid(self):
		return self._aminoAcid
	@aminoAcid.setter
	def aminoAcid(self, value):
		self._aminoAcid = value

	@property
	def composition(self):
		return deepcopy(self._composition)
	@composition.setter
	def composition(self, value):
		self._composition = deepcopy(value)
		self.UpdateMasses()

	@property
	def monoMass(self):
		return self._monoMass

	@property
	def avgMass(self):
		return self._avgMass

	@property
	def probability(self):
		return self._probability

	@probability.setter
	def probability(self, value):
		self._probability = value 

	##functions##
	def UpdateMasses(self):
		self._monoMass = 0
		self._avgMass = 0
		for atom in self._composition:
			self._monoMass += mono_masses[atom]
			self._avgMass += avg_masses[atom]
				
class AminoAcid(object):
	def __init__(self, abbreviation, monoMass, avgMass, diffMod = None):
		'''monoMass and avgMass are masses of the amino acid, w/o the diff mod'''
		self._abbreviation = abbreviation
		self._monoMass = monoMass
		self._avgMass = avgMass
		if diffMod is None:
			self._hasDiffMod = False
			self._diffMod = None
			self._diffModMonoMass = 0
			self._diffModAvgMass = 0
		else:
			self._hasDiffMod = True
			self._diffMod = deepcopy(diffMod)
			self._diffModMonoMass = diffMod.monoMass
			self._diffModAvgMass = diffMod.avgMass
	
	#properties
	@property
	def abbreviation(self):
		return self._abbreviation
	@abbreviation.setter
	def abbreviation(self, value):
		self._abbreviation = value
	
	@property
	def monoMass(self):
		return self._monoMass + self._diffModMonoMass
	@property
	def avgMass(self):
		return self._avgMass + self._diffModAvgMass
	
	@property
	def diffMod(self):
		if self._hasDiffMod:
			return deepcopy(self._diffMod)
		else:
			return self._diffMod

class Peak(object):
	def __init__(self, mz, intensity):
		self.mz = mz
		self.intensity = intensity
		self.index = -1
	def __cmp__(self, other):
		return cmp(self.mz, other.mz)



#!/bin/env python
import random


############################################################################
def shuffle_seq(seq=None, seed=None):
    """ Does an in-place Fisher-Yates shuffle of a sequence

    exchange each element in an array l of size n,
    starting at l[n-1], going down to 0, let the current index be i
    pick a random index from 0 to i to swap with i.

    This uses python builtin random library, which uses a Wichmann-Hill generator.
    The seed is automatically created from the system time,
    and the generator has a period of ~ 7e12.
    """
    if seq is None:
        return None
    else:
        l = list(seq)
        random.seed(seed)
        # i ranges from the last index to zero, decrementing by 1
        for i in range(len(l)-1,0,-1) :
            # j (swap index) goes from 0 to i
            j = int(random.random() * (i+1))
            # swap indices i,j
            if i == j:
                continue
            else:
                (l[i], l[j]) = (l[j], l[i])

        return tuple(l)


############################################################################
def pep_shuffle_old(pepSeq, i):

    prefix = pepSeq[:i:]
    midOne = pepSeq[i:i+1:]
    midTwo = pepSeq[i+1:i+2:]
    suffix = pepSeq[i+2:-1:]
    shuffledSeq = midTwo + prefix + suffix + midOne + pepSeq[-1]

    assert(len(shuffledSeq) == len(pepSeq))
    return shuffledSeq


############################################################################
def pep_shuffle(pepSeq, diffMasses=None, seed=None):
    cTerm = pepSeq[-1]
    shuffledDiffMasses = None
    if cTerm in ['R', 'K']:
        prefix = pepSeq[:-1]
        shuffledSeq = ''.join(shuffle_seq(prefix, seed)) + cTerm
        if diffMasses is not None:
            shuffledDiffMasses = shuffle_seq(diffMasses[:-1], seed) + tuple([diffMasses[-1]])
    else:
        shuffledSeq = ''.join(shuffle_seq(pepSeq, seed))
        if diffMasses is not None:
            shuffledDiffMasses = shuffle_seq(diffMasses, seed)

    assert(len(shuffledSeq) == len(pepSeq))

    return shuffledSeq, shuffledDiffMasses

############################################################################
def pep_reverse(pepSeq):
    
    prefix = pepSeq[:len(pepSeq)-1:]
    revSeq = "%s%s" % ( prefix[::-1],pepSeq[-1] )

    assert(len(revSeq) == len(pepSeq))
    return revSeq

############################################################################    
def pep_shift(pepSeq, i):
    
    prefix = pepSeq[:len(pepSeq)-1:]
    shifted = prefix[i::]+prefix[:i:]+pepSeq[-1]
    assert(len(shifted) == len(pepSeq))
    return shifted


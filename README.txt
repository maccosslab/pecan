================
PECAN 
================

[INTRODUCTION]

PECAN is a peptide-centric analysis tool for peptide detection directly from
DIA data without the need of a spectral library.

PECAN is Copyright(c) 2014 University of Washington. All rights reserved.
Written by Y. Sonia Ting, Michael J. MacCoss, in the Department of Genome
Sciences at the University of Washington. 


This tool takes a list of peptides and query each peptide against the DIA data
( in centroid mzML format) and finds the best top N chromotagraphic peaks(s)
for each peptide. 


[CITATION]

Please cite us when using PECAN for your publications.


[INSTALLATION]

python setup.py install

(you might need admin privileges to write in the python site-package folder)

[LOCAL INSTALLATION]

Python (as of 2.7 and 3.0) searches in the ~/.local directory for local
installs, which do not require administrative privilages to install. 


python setup.py install --user



[DOCUMENTATION]

For more in depth documentation of the modules and examples, please refer to
the documentation folder or http://github.com/pymzml



# PECAN

## INTRODUCTION
PECAN (PEptide-Centric Analysis) is a tool for peptide detection directly from DIA data without the need of a spectral library.
PECAN takes a list of peptide sequences and query each peptide against the DIA data (in centroid mzML format) and reports the best evidence of detection for each peptide. Percolator is used for false discovery rate FDR control.


## REQUIREMENT
python package

1. [pymzml][pymzml-github] version 0.7.4 or newer
2. [numpy][numpy-org] version 1.5 or newer

other dependencies
1. [percolator][percolator-ms] version 2.08 or newer

## INSTALLATION
`python setup.py install`

user might need admin privileges to write in the python site-package folder

## LOCAL INSTALLATION
`python setup.py install --user`

Python (as of 2.7) searches in the ~/.local directory for local installs, which do not require administrative privileges to install.


## DOCUMENTATION
For more in depth documentation of the modules and examples, please refer to the [tutorial][tutorial-v099]

## CITATION
Please cite us when using PECAN for your publications.

## COPYRIGHT NOTICE
PECAN is Copyright(c) 2016 University of Washington under [GNU lesser general public license][gnu]. All rights reserved.
Written by Y. Sonia Ting and Michael J. MacCoss, in the Department of Genome Sciences at the University of Washington.


[pymzml-github]: http://github.com/pymzml
[numpy-org]: http://www.numpy.org
[percolator-ms]: http://percolator.ms
[gnu]: LICENSE.txt
[tutorial-v099]: /docs/PECANTutorial_v0.9.9.pdf
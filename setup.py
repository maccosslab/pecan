#!/usr/bin/env python

from setuptools import setup

setup(name='PECAN',
      version='0.9.9.3',
      packages=['PECAN'],

      package_dir={'PECAN': 'PECAN'},
      package_data={'PECAN': ['PecanUtil/*']},
      
      scripts=['bin/pecan', 'bin/pecanpie', 'bin/pecan2blib'],
    

      description='''A peptide-centric analysis tool for peptide detection''',
      long_description=open("README.txt").read(),
      
      author='Y. Sonia Ting & Michael J. MacCoss',
      author_email='sonia810@uw.edu',
      url='http://pecan.maccosslab.org',
      
      license='LICENCES.txt',
      platforms='any that supports python 2.7',
      
      install_requires=[
            "numpy >= 1.5",
            "pymzml >= 0.7.4",
            ],
    
      classifiers=[
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Intended Audience :: Science/Research',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: GNU General Public License (GPL)',
            'Operating System :: MacOS :: MacOS X',
            'Operating System :: Unix',
            'Programming Language :: Python :: 2.7',
            'Topic :: Scientific/Engineering :: Bio-Informatics',
            ],
      )




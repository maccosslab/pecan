#!/usr/bin/env python
import sys
import logging
import re
import csv
import numpy as np
import operator
import collections
import argparse
from datetime import datetime
from subprocess import Popen, PIPE
SSRCalc = '/data/pecan/sgeadmin/SSRCalc3.pl'


class SSRCalculator():
    def __init__(self, referenceFileName=None):
        self._SSRIndeies = dict()
        if referenceFileName:
            referenceFile = csv.reader(open(referenceFileName, 'rb'), delimiter='\t')
            for row in referenceFile:
                seq=row[0]
                ssrIndex=float(row[2])
                self._SSRIndeies[seq] = ssrIndex

    def _calculateSSRIndex(self, sequenceList):
        querySeq = "/".join(sequenceList)
        p = Popen([SSRCalc, '--alg', '3.0', '--output', 'tsv', '--A', '0', '--B', '0', '--seq', querySeq],
                  stdout=PIPE,
                  close_fds=True)
        ssrOutputs = p.stdout.readlines()
        p.stdout.close()
        for line in ssrOutputs:
            try:
                tmp = line.split('\t')
                seq = tmp[0]
                hydrophobicity = float(tmp[2])
                self._SSRIndeies[seq] = hydrophobicity
            except IndexError:
                continue
        sys.stderr.write('SSRIndex Dict Size: %s\n' % len(self._SSRIndeies))

    def GetSSRIndex(self, sequence):
        try:
            ssrIndex = self._SSRIndeies[sequence]
        except KeyError:
            self._calculateSSRIndex([sequence])
            try:
                ssrIndex = self._SSRIndeies[sequence]
            except KeyError:
                ssrIndex = 'nan'
        finally:
            return ssrIndex

    def CalSSRIndexFromList(self, sequenceList):
        for seq in sequenceList:
            if seq in self._SSRIndeies:
                sequenceList.remove(seq)
        self._calculateSSRIndex(sequenceList)


class RetentionTimeCalibrator():
    def __init__(self, tList, dList, calculator=None):
        self._rankTList = sorted(tList, key=operator.attrgetter('score'), reverse=True)
        self._rankDList = sorted(dList, key=operator.attrgetter('score'), reverse=True)
        self._m = None
        self._c = None
        self._SSRTimes = {}
        self._pValues = {} 
        if calculator is None:
            self._calculator = SSRCalculator()
        else:
            self._calculator = calculator

    def CalibrateFromScore(self, calibrationSize=None):
        if calibrationSize is None:
            calibrationSize = 0
            for tIndex in xrange(0, len(self._rankTList)):
                if self._pValues[self._rankTList[tIndex].score] <= 0.01:
                    calibrationSize += 1
            logger.info('p-value < 0.01 SSR calibration size = %s', calibrationSize)
            
            if calibrationSize < 20:
                calibrationSize = 0
                for tIndex in xrange(0, len(self._rankTList)):
                    if self._pValues[self._rankTList[tIndex].score] <= 0.05:
                        calibrationSize += 1
                logger.info('p-value < 0.05 SSR calibration size = %s', calibrationSize)        

            if calibrationSize < 20:
                calibrationSize = 20

        logger.info('SSR calibration size = %s', calibrationSize)
        
        hydrophobicities = []
        bestPeakTimes = []
        for rank in xrange(0, calibrationSize):
            p = self._rankTList[rank]
            ssrIndex = self._calculator.GetSSRIndex(p.sequence)
            if ssrIndex != 'nan':
                hydrophobicities.append(ssrIndex)
                bestPeakTimes.append(p.retentionTime)
        self.Calibrate(hydrophobicities, bestPeakTimes)

    def Calibrate(self, hydrophobicities, retentionTimes):
        x = np.array(hydrophobicities)
        y = np.array(retentionTimes)
        A = np.vstack([x, np.ones(len(x))]).T
        self._m, self._c = np.linalg.lstsq(A, y)[0]
        logger.info('SSR calibration m=%f\tc=%f', self._m, self._c)
        self.Propergate()

    def CalibrateFromPercolator(self, percolatorRsltFileName, qValueCutoff=0.01, protTag=None):
        sys.stderr.write("===== Reading file %s\n" % percolatorRsltFileName)
        percRsltFile = csv.DictReader(open(percolatorRsltFileName, 'rb'), delimiter='\t')
        hydrophobicities = []
        bestPeakTimes = []
        for row in percRsltFile:
            qValue = float(row.get('q-value'))
            if qValue >= qValueCutoff:
                continue
            if protTag is not None:
                proteinIds = row.get('proteinIds')
                if re.search(protTag, proteinIds) is None:
                    continue
            rTime = float(row.get('PSMId').split(':')[1])
            seq = row.get('sequence').split('.')[1]

            ssrIndex = self._calculator.GetSSRIndex(seq)
            if ssrIndex != 'nan':
                hydrophobicities.append(ssrIndex)
                bestPeakTimes.append(float(rTime))

        self.Calibrate(hydrophobicities, bestPeakTimes)

    def Propergate(self):
        for result in self._rankTList + self._rankDList:
            ssrIndex = self._calculator.GetSSRIndex(result.sequence)
            if ssrIndex == 'nan':
                ssrIndex = 0
            SSRtime = m*ssrIndex + c
            self._SSRTimes[result.id] = abs(SSRtime - result.retentionTime)

    def CalculatePvalue(self):
        decoyNumber = len(self._rankDList)
        nD = 0      # number of decoys above the target score
        iD = 0      # index of current decoy location

        for j in xrange(0, len(self._rankTList)):
            targetScore = self._rankTList[j].score
            for i in xrange(iD, decoyNumber):
                decoyScore = self._rankDList[i].score
                if targetScore > decoyScore:
                    p = float(i)/decoyNumber
                    self._pValues[targetScore] = p
                    iD = i
                    break
                elif i == (decoyNumber-1):
                    self._pValues[targetScore] = 1
                    break

    def GetSSRTimes(self):
        return self._SSRTimes


####################################################################
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''

            PecanRTCalibrator.py <Pecan.feature> <score name>

            ''',
            prog = 'PecanRTCalibrator.py',
            formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('pecanFeatureFileName', help='path to the pecan generated .feature file')
    parser.add_argument('scoreName', help='name of the column in the feature file that will be used to calculate '
                                          'p-values for RT calibration')
    parser.add_argument('-r', dest='referenceFileName', type=str, default=None,
                        help='path to SSRIndex reference tsv\n')
    parser.add_argument('-p', dest='percolatorRsltFileName', type=str, default=None,
                        help='path to percolator rsults tsv\nwhen provided will use q-value for RT calibration\n')

    args = parser.parse_args()
    pecanFeatureFileName = args.pecanFeatureFileName
    scoreName = args.scoreName

    fileRoot = re.sub('.feature',  '', pecanFeatureFileName)
    # Log
    logFileName = "%s.RTCalibrate.log" % fileRoot 
    logger = logging.getLogger('PecanRT')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(logFileName)
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s| %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.WARNING)
    logger.addHandler(ch)

    startTime = datetime.now()
    logger.info("commandLine: %s", ' '.join(sys.argv))
    logger.info("startTime: %s", startTime)

    # Load the feature file
    logger.warning('====== Loading %s', pecanFeatureFileName)
    pecanFeatureFile = csv.DictReader(open(pecanFeatureFileName, 'rb'), delimiter='\t')
    PecanFeature = collections.namedtuple('PecanFeature', 'id sequence score retentionTime')
    tList = []
    dList = []
    seqList = []
    for row in pecanFeatureFile:
        seq = row.get('sequence').split(".")[1]
        seqList.append(seq)
        charge = int(row.get('id').split("+")[1])
        td = int(row.get('TD'))
        f = PecanFeature(id=row.get('id'),
                         sequence=seq,
                         score=float(row.get(scoreName)),
                         retentionTime=float(row.get('retentionTime')))
        if td == 1:
            tList.append(f)
        elif td == -1:
            dList.append(f)
        else:
            logger.warning('Unknown TD tag!')
    logger.warning('====== Calculating SSRIndex in batch')
    calculator = SSRCalculator(args.referenceFileName)
    batchSize = 5000
    for i in xrange(0, len(seqList), batchSize):
        j = i+batchSize
        if j > len(seqList):
            j = len(seqList)
        calculator.CalSSRIndexFromList(seqList[i:j])

    logger.warning('====== Calibrating retention time')
    rtCalibrator = RetentionTimeCalibrator(tList, dList, calculator)
    if args.percolatorRsltFileName:
        rtCalibrator.CalibrateFromPercolator(args.percolatorRsltFileName, qValueCutoff=0.01)
    else:
        rtCalibrator.CalculatePvalue()
        rtCalibrator.CalibrateFromScore()
    SSRTimes = rtCalibrator.GetSSRTimes() 
    deltaTime = datetime.now()-startTime
    startTime = datetime.now()
    logger.warning('====== DURATION: %s seconds ======', deltaTime.seconds)

    columns = pecanFeatureFile.fieldnames
    columns.insert(17, 'deltaTime')

    # Write the new features with deltaTime  
    outFileName = "%s.RT.feature" % fileRoot
    logger.warning('====== Writing %s', outFileName)
    outFile = csv.DictWriter(open(outFileName, 'wb'),  fieldnames=columns, delimiter='\t')
    pecanFeatureFile = csv.DictReader(open(pecanFeatureFileName, 'rb'), delimiter='\t')
    
    outFile.writeheader()
    for row in pecanFeatureFile:
        row['deltaTime'] = "%.2f" % SSRTimes[row.get('id')]
        if row['precursorMassErrMean'] == 'nan':
            row['precursorMassErrMean'] = 0.0
        outFile.writerow(row)








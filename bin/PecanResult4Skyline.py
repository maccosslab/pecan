#!/bin/env python
import sys
import operator
import argparse
import csv
import collections
import re

#############################################################
# Read the list of targets and scores from percolator results
def readPercolatorFile ( fileName, extention=None, qValue=None):

    sys.stderr.write("===== Reading file %s\n" % fileName )
    numEntry = 0
    PercResult = collections.namedtuple('PercResult', 'PeptideSequence FileName RetentionTime Score')    

    percRsltFile = csv.DictReader(open( fileName , 'rb'), delimiter='\t')
    rList = []
    for row in percRsltFile:
        seq = row.get('peptide').split(".")[1]
        q = float( row.get('q-value')) 
        if q >= qValue: 
            continue            
        rawFile = row.get('proteinIds').split(":")[0] 
        rTime   = row.get('proteinIds').split(":")[1]
        if extention:
            rawFile = re.sub( 'mzML', extention ,rawFile) 
        
        rList.append( PercResult(PeptideSequence=seq, FileName=rawFile, RetentionTime=rTime, Score=q)  )
    
    return rList

#############################################################
# Read the list of targets and scores from percolator results
def readPercolatorFile4PB ( fileName, extention=None, qValue=None):

    sys.stderr.write("===== Reading file %s\n" % fileName )
    numEntry = 0
    PercResult = collections.namedtuple('PercResult', 'PeptideSequence PeptideModifiedSequence FileName PrecursorCharge MinStartTime MaxEndTime PecanQValue')    

    percRsltFile = csv.DictReader(open( fileName , 'rb'), delimiter='\t')
    rList = []
    for row in percRsltFile:
        seq = row.get('peptide').split(".")[1]
        pepModSeq = re.sub("C", "C[+57]", seq)
        charge = row.get('PSMId')[-1::]
        q = float( row.get('q-value')) 
        if q >= qValue: 
            continue            
        rawFile = row.get('proteinIds').split(":")[0] 
        rTime   = float(row.get('proteinIds').split(":")[1])
        sTime   = "%.2f" % (rTime-0.1)
        eTime   = "%.2f" % (rTime+0.1)
        if extention:
            rawFile = re.sub( 'mzML', extention ,rawFile) 
        
        rList.append( PercResult(PeptideSequence=seq, PeptideModifiedSequence=pepModSeq, FileName=rawFile, PrecursorCharge=charge, MinStartTime=sTime, MaxEndTime=eTime, PecanQValue=q)  )
    
    return rList

    


############################################################################
# MAIN
#############################################################################


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''
        Parse percolator target result generated from Pecan feature file, 
        where annotation is xxx.raw:peakTime into Skyline compatable tsv 
        table for performance comparsion. If the percolater result is 
        generated from standard SEQUEST output (i.e. not Pecan, a ms2.header
        file is required to pull the retention time information. 


        Output format 1: 
        PeptideSequence  FileName  RetentionTime  Score(q-value)
        PEPTIDER  xxx.raw  45.7  0.098
        SEQUENCER xxx.raw  40.3  0.0001

        Output format 2: (when --PB is used) 
        FileName  PeptideSequence  PeptideModifiedSequence  PrecursorCharge  MinStartTime  MaxEndTime  PeptideRetentionTime
        xxx.raw   VDFHAQWCGPCK     VDFHAQWC[+57]GPC[+57]K   3                46.92         47.54       47.23
        xxx.raw   PEPTIDER         PEPTIDER                 3                40.93         41.33       41.01   

        
        ''', 
        prog = 'PecanResult4Skyline.py',
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('percRsltFileName', help='path to the percolator result table')
    parser.add_argument('outputRoot', help='name base of the output tsv file')

    parser.add_argument('-e', '--extention', type=str, help='raw file extention if not mzML')    
    parser.add_argument('-m', '--ms2Header', type=str, help='path to the ms2.header file if needed')
    parser.add_argument('--PB', action='store_true', help='output peak boundries for Skyline')
    parser.add_argument('-q', '--qValue', type=float, help='q-value cut off')
    parser.add_argument('-l', '--peptideList', type=str, help='list of peptide sequences queried in Skyline')

    args = parser.parse_args()
    
    if args.qValue:
        qValue=args.qValue
    else:
        qValue=1.1
   
    
    if args.peptideList:
        peptides = []
        pepFile= open( args.peptideList , "r")
        for line in pepFile:
            peptides.append( line.rstrip().split('\t')[0])        
        
    if args.PB:
        outputFileName = "%s.PB.csv" % args.outputRoot
        percRslt= readPercolatorFile4PB( args.percRsltFileName , args.extention, qValue)
        outFile = csv.writer( open(outputFileName, 'wb'))
        outFile.writerow(percRslt[0]._fields)
        for p in sorted( percRslt, key=lambda x: x.PeptideSequence):
            if not args.peptideList:
                outFile.writerow( p)
            else:
                if p.PeptideSequence in peptides:
                    outFile.writerow( p)

    else:
        outputFileName = "%s.tsv" % args.outputRoot
        percRslt= readPercolatorFile( args.percRsltFileName , args.extention, qValue)
    
        outFile = csv.writer( open(outputFileName, 'wb'),  delimiter='\t')
        outFile.writerow(percRslt[0]._fields)
        for p in sorted( percRslt, key=lambda x: x.PeptideSequence):
            outFile.writerow( p)



    




#!/usr/bin/env python

import os
import sys
import re
import argparse
import collections
import time
import csv


timeStamp = hex(int((time.time()-int(time.time()))*1000000))[:6]
outDirRoot = "PECANJobs_%s" % timeStamp
cwd = os.getcwd()

### PECAN defaults
isoWindowWidth = 5.0
ms1ppmTolerance = 10
ms2ppmTolerance = 10
minElute = 6
maxElute = minElute +1
bgDecoyNumber = 2000
minCharge = 2
maxCharge = 3
idotpThreshold = 0.0
topX = 3
bgDenominator=1 
overlapPercentage = 0
backgroundProteome = None

### 
#pecan = "Pecan.py";
pecan = "/net/maccoss/vol5/home/sonia810/proj/svn/PECAN/Pecan.py"
parameters = []


###
def writeHeader (jobFile, jobDirectory, mzML, backgroundProteome ):

    jobFile.write("#$ -S /bin/bash\n")
    jobFile.write("#$ -cwd\n\n")
    jobFile.write("source $HOME/.bashrc\n\n")
    
    jobFile.write("date\n\n")

    jobFile.write("echo $TMPDIR\n")
    jobFile.write("echo 'PECAN job directory = %s'\n" % jobDirectory)
    jobFile.write("echo 'mzMLFile = %s'\n" %  mzML.path)
    jobFile.write("cp %s $TMPDIR/%s\n" %    ( mzML.path, mzML.name))

    jobFile.write("echo 'backgroundProteome.txt = %s'\n" % backgroundProteome)
    if backgroundProteome:
        jobFile.write("cp %s $TMPDIR/backgroundProteome.txt\n\n" % backgroundProteome)


###
def writeQueryPeptides (jobFile, peptideFile ):
    jobFile.write("echo 'queryPeptide = %s'\n" % peptideFile.path )
    jobFile.write("cp %s $TMPDIR/%s\n"       % ( peptideFile.path, peptideFile.name ))


###
def tarResults ( jobFile, root): 
         
    jobFile.write("\n\necho 'Combining results'\n") 
    
    jobFile.write("tar -czf $TMPDIR/%s.PECAN.tdfs.tgz $TMPDIR/%s*td.feature\n" % (root, root))
    jobFile.write("cp $TMPDIR/%s.PECAN.tdfs.tgz ." % root )

    jobFile.write("tar -czf $TMPDIR/%s.PECAN.logs.tgz $TMPDIR/%s*.log\n" % (root, root))
    jobFile.write("cp $TMPDIR/%s.PECAN.logs.tgz ." % root )
    
    jobFile.write("featureFile=%s.Pecan.feature\n" % root )
    jobFile.write("\nfor tdf in $TMPDIR/%s*.td.feature\ndo\n" % root) 
    jobFile.write("\tif [ ! -e $featureFile ]; then\n")
    jobFile.write("\t\tawk 'NR==1' $tdf > $featureFile\n")
    jobFile.write("\tfi\n\n")
    jobFile.write("\tawk 'NR>1' $tdf >> $featureFile\ndone\n\n")
        

############################################################################
# MAIN
#############################################################################

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''
            
            
        ''', 
        prog="PecanJobsGenerator.py", 
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('mzMLFileNameList', help='Path to the list of centroid .mzML files' )
    parser.add_argument('peptideListFileNameList', help='Path to the list of query peptide lists')
    parser.add_argument('isolationWindowList', type=str, help='List of target precursor m/z')
    
    parser.add_argument('-o', dest='jobDirRoot', type=str, default=outDirRoot, 
                        help='Output directory root for PECAN jobs\nDefault= %s\n\n' % outDirRoot )
    parser.add_argument('-s', dest='species', type=str,
                        help='Use default species proteome as background proteome\n(None/human/yeast/mouse)\nDefault= None\n\n')
    
    ### Job spliting 
    #parser.add_argument('--split-by-charge', action='store_true', help='Split jobs by charge state\n\n')
    parser.add_argument('--splitByIsoWindow', action='store_true', help='Split jobs by isolation window\n\n')
    parser.add_argument('--tar', action='store_true', help='tar the results (features and logs) per job\n\n')    

    ### Setting PECAN parameters
    ### PECAN peptide / proteome settings
    parser.add_argument('-b', '--backgroundProteome', type=str,
                        help='Peptide list file name of background proteome\n(Overwrites -s SPECIES)\nDefault= None\n\n')
    
    parser.add_argument('--minCharge', type=int, default= minCharge,
                        help='Minimum charge state searched\nDefault= %s\n\n' % minCharge)

    parser.add_argument('--maxCharge', type=int, default= maxCharge,
                        help='Maximum charge state searched\nDefault= %s\n\n' % maxCharge)


    ### PECAN MS/MS experiment settings
    parser.add_argument('-w', '--isoWindowWidth', type=float, 
                        help='Isolation window width used in acquisition\nDefault= %.2f Th\n\n' % isoWindowWidth)
    
    parser.add_argument('-p', '--ms1ppm', type=int,
                        help='Mass error for precursor ion extracting\nDefault=%d ppm\n\n' % ms1ppmTolerance)
    
    parser.add_argument('-q', '--ms2ppm', type=int,
                        help='Mass error for fragment ion extracting\nDefault=%d ppm\n\n' % ms2ppmTolerance)

    parser.add_argument('--overlap',type=int, 
                        help='Overlap percentage of isolation window\nDefault= %d \n\n' % overlapPercentage)

    parser.add_argument('--SWATH', action='store_true', help='Select if mzML is generated from AB Sciex converter')


    ### PECAN peak peaking / scoring settings
    parser.add_argument('-t', '--topX', type=int, 
                        help='Reporting top x peaks per charged peptide\nDefault= %d\n\n' % topX)
    
    parser.add_argument('-z', dest='bgDenominator', type=int,
                        help='Denominator for background scores\nDefault= %d\n\n' % bgDenominator)

    parser.add_argument('-m', '--minElution', type=int, 
                        help='Minimum number of cycles expected a peptide to elute\nDefault= %d scan\n\n' % minElute)

    parser.add_argument('-x', '--maxElution', type=int,
                        help='Maximum number of cycles expected a peptide to elute\nDefault= %d scan\n\n' % maxElute)

    parser.add_argument('-i','--idotp', type=float, 
                        help='MS1 idotp threshold used for peak picking\nDefault= %f\n\n' % idotpThreshold)

    args = parser.parse_args()

   
    #### Processing mzML files / query peptide lists / isolaiton windows 
    File=collections.namedtuple('File', 'path name label')
    mzMLFiles = []
    if not os.path.isfile(args.mzMLFileNameList):
        sys.stderr.write("No such file: %s\n" % args.mzMLFileNameList)
        sys.exit()
    else:
        with open(args.mzMLFileNameList, 'r') as mzMLFileNameList:
            for line in mzMLFileNameList:
                path = line.rstrip() 
                name = path.split('/')[-1]
                label = re.sub('.mzML', '.pecan', name)
                mzMLFiles.append( File(path, name, label) )


    peptideListFiles = []
    if not os.path.isfile(args.peptideListFileNameList):
        sys.stderr.write("No such file: %s\n" % args.peptideListFileNameList)
        sys.exit()
    else:
        with open(args.peptideListFileNameList, 'r') as peptideListFileNameList:
            for line in peptideListFileNameList:
                path = line.rstrip()
                name = path.split('/')[-1]
                label = "p%s" % (len(peptideListFiles)+1)
                peptideListFiles.append( File(path, name, label) )


    isolationWindows = []    
    if not os.path.isfile(args.isolationWindowList):
        sys.stderr.write("No such file: %s\n" % args.isolationWindowList)
        sys.exit()
    else:
        with open(args.isolationWindowList, 'r') as isolationWindowList:
            for line in isolationWindowList:
                isolationWindows.append( line.rstrip() )

    #### setting background proteme 
    if args.species:
        if args.species.upper() == "YEAST":
            backgroundProteome = "/net/maccoss/vol5/home/sonia810/proj/PeptideCentricAnalysis/database/yeast-030211-contambov6/peptides.txt"             
        elif args.species.upper() == "MOUSE":
            backgroundProteome = "/net/maccoss/vol5/home/sonia810/proj/HF/database/mouse-uniprot-290313-contam-index/peptides.txt"
        elif args.species.upper() == "HUMAN": 
            backgroundProteome = "/net/maccoss/vol5/home/sonia810/proj/PeptideCentricAnalysis/database/human-ipi-250309-contam/peptides.txt"
        else:
            sys.stderr.write("Unknown species %s\n" % args.species)

    if args.backgroundProteome:
        if not os.path.isfile( args.backgroundProteome):
            sys.stderr.write("No such file: %s" % args.backgroundProteome)
            sys.exit()
        backgroundProteome = args.backgroundProteome

    ## parameters
    if args.minCharge:
        parameters.append("--minCharge %s" % args.minCharge)
    
    if args.maxCharge:
        if args.maxCharge < args.minCharge:
            args.maxCharge = args.minCharge 
        parameters.append("--maxCharge %s" % args.maxCharge)

    if args.isoWindowWidth:
        parameters.append("--isoWindowWidth %s" % args.isoWindowWidth)

    if args.ms1ppm:
        parameters.append("--ms1ppm %s" % args.ms1ppm)

    if args.ms2ppm:
        parameters.append("--ms2ppm %s" % args.ms2ppm)

    if args.overlap:
        parameters.append("--overlap %s" % args.overlap)

    if args.SWATH:
        parameters.append("--SWATH")

    if args.topX:
        parameters.append("--topX %s" % args.topX)

    if args.bgDenominator:
        parameters.append("-z %s" % args.bgDenominator)

    if args.minElution:
        parameters.append("--minElution %s" % args.minElution)

    if args.maxElution:
        parameters.append("--maxElution %s" % args.maxElution)

    if args.idotp:
        parameters.append("--idotp %s", args.idotp)


    
    if backgroundProteome:
        parameters.append("--backgroundProteome $TMPDIR/backgroundProteome.txt") 
    else:
        parameters.append("--backgroundProteome None")

    jobDirectory = "%s/%s" % (cwd, args.jobDirRoot ) 
    if not os.path.exists( jobDirectory ):
        os.makedirs(jobDirectory)  
        sys.stderr.write("Creating PECAN job directory: %s\n" % jobDirectory)
    
    ### check if the job directory can see the files
    os.chdir(jobDirectory)
    for mzMLFile in mzMLFiles: 
        if not os.path.isfile( mzMLFile.path ):
            sys.stderr.write("No such file: %s\n" % mzMLFile.path )
            sys.exit()
   
    for peptideFile in peptideListFiles: 
        if not os.path.isfile( peptideFile.path ):
            sys.stderr.write("No such file: %s\n" % peptideFile.path )
            sys.exit()


    ### Generating job script 
    pecanParams = " ".join(parameters)
    for mzMLFile in mzMLFiles:
        
        if args.splitByIsoWindow:
            sys.stderr.write("Splitting jobs by isolation windows\n")
            for i in isolationWindows: 
                name = "%s.%s.job" % (mzMLFile.label, i)
                if not name[0].isalpha():
                    name = "P%s" % name
                path = "%s/%s" % ( jobDirectory, name)
                label = re.sub('.job', '', name)
                job = File( path, name, label) 
                
                with open( job.path, 'wb') as jobFile:
                    writeHeader (jobFile, jobDirectory, mzMLFile, backgroundProteome )

                    if len(peptideListFiles) == 1:
                        peptideFile = peptideListFiles[0]
                        writeQueryPeptides(jobFile, peptideFile)

                        featureFileName = "%s.%s.td.feature" % (mzMLFile.label, i )
                        logFileName = "%s.%s.log" % (mzMLFile.label, i )
                        if os.path.isfile( featureFileName ):
                            sys.stderr.write('%s exists! Skipping!\n' % featureFileName)
                        else:
                            jobFile.write("\n\n%s %s $TMPDIR/%s $TMPDIR/%s %s $TMPDIR/%s" % (pecan, pecanParams, mzMLFile.name, peptideFile.name, i, mzMLFile.label ))
                            jobFile.write("\nif [ ! -e $TMPDIR/%s ]; then\n\tcp $TMPDIR/%s %s" % (featureFileName, logFileName, jobDirectory))
                            jobFile.write("\nelse\n\tcp $TMPDIR/%s %s\nfi" % ( featureFileName, jobDirectory))
                    else:
                        for peptideFile in peptideListFiles:
                            writeQueryPeptides(jobFile, peptideFile) 
                        
                        for peptideFile in peptideListFiles:
                            featureFileName = "%s.%s.%s.td.feature" % (mzMLFile.label, peptideFile.label, i )
                            logFileName = "%s.%s.%s.log" % (mzMLFile.label, peptideFile.label, i )
                            if os.path.isfile( featureFileName ):
                                sys.stderr.write('%s exists! Skipping!\n' % featureFileName)
                            else:
                                jobFile.write("\n\n%s %s $TMPDIR/%s $TMPDIR/%s %s $TMPDIR/%s.%s" % 
                                             (pecan, pecanParams, mzMLFile.name, peptideFile.name, i, mzMLFile.label, peptideFile.label))
                                jobFile.write("\nif [ ! -e $TMPDIR/%s ]; then\n\tcp $TMPDIR/%s %s" % (featureFileName, logFileName, jobDirectory))
                                jobFile.write("\nelse\n\tcp $TMPDIR/%s %s\nfi" % ( featureFileName, jobDirectory))

                                #jobFile.write("\ncp $TMPDIR/%s %s" % ( featureFileName, jobDirectory))        
                    jobFile.write("\n\necho 'Job %s complete!'\n" % job.name )

        else:
            name = "%s.job" % (mzMLFile.label)
            if not name[0].isalpha():
                name = "P%s" % name
            path = "%s/%s" % ( jobDirectory, name)
            label = re.sub('.job', '', name)
            job = File( path, name, label)
            
            with open( job.path, 'wb') as jobFile:
                writeHeader (jobFile, jobDirectory, mzMLFile, backgroundProteome )
                if len(peptideListFiles) == 1:
                    peptideFile = peptideListFiles[0]
                    writeQueryPeptides(jobFile, peptideFile)
                    for i in isolationWindows:
                        featureFileName = "%s.%s.td.feature" % (mzMLFile.label, i )
                        logFileName = "%s.%s.log" % (mzMLFile.label, i )
                        if os.path.isfile( featureFileName ):
                            sys.stderr.write('%s exists! Skipping!\n' % featureFileName)
                        else:
                            jobFile.write("\n\n%s %s $TMPDIR/%s $TMPDIR/%s %s $TMPDIR/%s" % 
                                         (pecan, pecanParams, mzMLFile.name, peptideFile.name, i, mzMLFile.label ))
                            jobFile.write("\nif [ ! -e $TMPDIR/%s ]; then\n\tcp $TMPDIR/%s %s" % (featureFileName, logFileName, jobDirectory))
                            jobFile.write("\nelse\n\tcp $TMPDIR/%s %s\nfi" % ( featureFileName, jobDirectory))

                            #jobFile.write("\ncp $TMPDIR/%s %s" % ( featureFileName, jobDirectory))
                else:
                    for peptideFile in peptideListFiles:
                        writeQueryPeptides(jobFile, peptideFile)
                    for peptideFile in peptideListFiles:
                        for i in isolationWindows:
                            featureFileName = "%s.%s.%s.td.feature" % (mzMLFile.label, peptideFile.label, i )
                            logFileName = "%s.%s.%s.log" % (mzMLFile.label, peptideFile.label, i )
                            if os.path.isfile( featureFileName ):
                                sys.stderr.write('%s exists! Skipping!\n' % featureFileName)
                            else:
                                jobFile.write("\n\n%s %s $TMPDIR/%s $TMPDIR/%s %s $TMPDIR/%s.%s" % 
                                             (pecan, pecanParams, mzMLFile.name, peptideFile.name, i, mzMLFile.label, peptideFile.label))
                                jobFile.write("\nif [ ! -e $TMPDIR/%s ]; then\n\tcp $TMPDIR/%s %s" % (featureFileName, logFileName, jobDirectory))
                                jobFile.write("\nelse\n\tcp $TMPDIR/%s %s\nfi" % ( featureFileName, jobDirectory))

                                #jobFile.write("\ncp $TMPDIR/%s %s" % ( featureFileName, jobDirectory))
                if args.tar:
                    tarResults(jobFile, job.label)
                jobFile.write("\n\necho 'Job %s complete!'\n" % job.name )
        
        jobFile.write("date")

    sys.exit()




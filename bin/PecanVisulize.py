#!/bin/env python
import sys
import operator
import argparse
import csv
import collections
import re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FuncFormatter
from matplotlib.lines import Line2D
from copy import copy

Stats = collections.namedtuple('Stats', 'correctPeaks incorrectPeaks noRef totalPeaks correctPeptides')


#############################################################
# Calculate peaks
def calPeakStats(peakList, peakReference):
    correctPicks = []
    incorrectPicks = []
    noRef = []
    refCorrect = {}
    for peak in peakList:
        try:
            pb = peakReference[peak.PeptideSequence][peak.FileName]
        except KeyError:
            continue

        refName = peak.FileName + peak.PeptideSequence

        if pb.MinStartTime == "#N/A":
            noRef.append(peak.Score)
        elif float(pb.MaxEndTime) >= peak.RetentionTime >= float(pb.MinStartTime):
            try:
                correctPicks.append(peak.Score)
            except AttributeError:
                correctPicks.append(peak)

            if refName not in refCorrect:
                refCorrect[refName] = 1
            else:
                refCorrect[refName] += 1
        else:
            try:
                incorrectPicks.append(peak.Score)
            except AttributeError:
                incorrectPicks.append(peak)

            if args.printIncorrects:
                sys.stdout.write("%s\t%s\t%s\n" % (peak.PeptideSequence, peak.FileName, peak.RetentionTime))

    totalPicked = len(correctPicks) + len(incorrectPicks)
    sys.stderr.write('===============================\nFile: %s\n' % exp)
    sys.stderr.write('Label: %s\n' % exp)
    sys.stderr.write('No valid reference: %s\n' % len(noRef))
    sys.stderr.write('Correct picks: %s\n' % len(correctPicks))
    sys.stderr.write('Incorrect picks: %s\n' % len(incorrectPicks))
    sys.stderr.write('Total picks: %s\n' % totalPicked)
    sys.stderr.write('refCorrect: %s\n' % len(refCorrect.keys()))
    stats = Stats(correctPeaks=len(correctPicks),
                  incorrectPeaks=len(incorrectPicks),
                  noRef=len(noRef),
                  totalPeaks=totalPicked,
                  correctPeptides=len(refCorrect.keys()))
    return stats, copy(correctPicks), copy(incorrectPicks)


#############################################################
# Read the list of targets and scores from percolator results
def readPercolatorFile(fileName, extension=None, qValueCutoff=1, protTag=None):

    sys.stderr.write("===== Reading file %s\n" % fileName )
    PercResult = collections.namedtuple('PercResult', 'PeptideSequence FileName RetentionTime Score')    

    percRsltFile = csv.DictReader(open(fileName, 'rb'), delimiter='\t')
    rList = []
    for row in percRsltFile:
        qValue = float(row.get('q-value'))
        if qValue >= qValueCutoff:
            continue
        if protTag is not None:
            proteinIds = row.get('proteinIds')
            if re.search(protTag, proteinIds) is None:
                continue
        rawFile, rTime, pid = row.get('PSMId').split(':')
        seq = pid.split('+')[0]
        if extension:
            rawFile = re.sub('mzML', extension, rawFile)
        
        rList.append(PercResult(PeptideSequence=seq, FileName=rawFile, RetentionTime=float(rTime), Score=qValue))
    return rList


#############################################################
# Read the retention time and filter with topN from pecan feature files
def readPecanFeatures(fileName, extension=None, topNcutoff=10, protTag=None):

    sys.stderr.write("===== Reading file %s\n" % fileName)
    PecanFeature = collections.namedtuple('PecanFeature', 'PeptideSequence FileName RetentionTime TopN')
    featureFile = csv.DictReader(open(fileName, 'rb'), delimiter='\t')
    rList = []
    for row in featureFile:
        if int(row.get('TD')) != 1:
            continue
        topN = int(row.get('topN'))
        if topN > topNcutoff:
            continue
        if protTag is not None:
            proteinIds = row.get('annotation')
            if re.search(protTag, proteinIds) is None:
                continue
        rawFile, rTime, pid = row.get('id').split(':')
        seq = pid.split('+')[0]
        if extension:
            rawFile = re.sub('mzML', extension, rawFile)

        rList.append(PecanFeature(PeptideSequence=seq, FileName=rawFile, RetentionTime=float(rTime), TopN=int(topN)))
    return rList


#############################################################
def setFigureLabelSize( ax, fontSize):
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize( fontSize)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize( fontSize)
    

#############################################################
def to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = "%d" % ( y*100 )

    # The percent symbol needs escaping in latex
    if matplotlib.rcParams['text.usetex'] == True:
        return s + r'$\%$'
    else:
        return s + '%'

############################################################################
# 
def calculatePvalue( targetSocres, decoyScores):

    decoyScores.sort(reverse=True)
    targetSocres.sort(reverse=True)
    numDecoy = len(decoyScores)
    
    pValue = {} # Key = targetScore , values = p-value
    #m = 0.005   # m estimate 
    nD = 0      # number of decoys above the target score
    iD = 0      # index of current decoy location 

    for s in targetSocres:
        for i in xrange(iD, numDecoy):
            scoreD = decoyScores[i]
            #sys.stderr.write("comparing score:%s === %s\n" % (s, scoreD) )
            if ( s > scoreD ):
                p = float(i)/float(numDecoy)
                pValue[s] = p
                iD = i
                break
            elif( i == (numDecoy-1)):
                pValue[s] = 1
                break


    return pValue
    
############################################################################
# FP/(FP+TP)
def calculateFDR( correctScores, incorrectScores):

    correctScores.sort()
    incorrectScores.sort()
    numCorrect = len(correctScores)
    numIncorrect = len(incorrectScores)

    FDRs = {}    # Key = correctScore, value = FDR

    #nC = 0      # number of correct score above the correct score
    iC = 0      # index of current correct score location 
    iIC = 0     # index of current incorrect score location
    fp = 0.0      # false positivies 
    tp = 0


    for iIC in xrange(0, numIncorrect):
        scoreIC = incorrectScores[iIC]
        for i in xrange(iC, numCorrect):
            scoreC = correctScores[i]
            #print "index=%s iIC=%s fp=%s correct=%s" % (iIC, scoreIC,fp, scoreC)
            if iIC == numIncorrect-1:
                FDRs[scoreC] = numIncorrect/(numIncorrect+i+1)
                
            elif scoreC <= scoreIC:
                fdr = fp/(fp+i+1) 
                FDRs[scoreC] = fdr
            
            elif scoreC > scoreIC:
                iC = i
                break 
            
            #print "FDR = %s= %s" % (scoreC, FDRs[scoreC] )
        fp +=1
         
    return FDRs



############################################################################
# MAIN
#############################################################################


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='''
        Plot Skyline compatable tsv table for performance comparsion. 

        Input peak picking result format: 
        PSMId	score	q-value	posterior_error_prob	peptide	proteinIds
        Q_2013_0129_RJ_11.mzML:45.89:IGLDCASSEFFK+2	6.69376	0	5.65946e-14	-.IGLDCASSEFFK.-	yeast_500
        Q_2013_0129_RJ_11.mzML:35.62:VNQIGTLSESIK+2	6.00106	0	1.0538e-12	-.VNQIGTLSESIK.-	yeast_500
        Q_2013_0129_RJ_11.mzML:61.29:SGETEDTFIADLVVGLR+3	4.60499	0	3.82201e-10	-.SGETEDTFIADLVVGLR.-	yeast_500
        Q_2013_0129_RJ_11.mzML:33.37:IGGIGTVPVGR+2	4.06772	0	3.69254e-09	-.IGGIGTVPVGR.-	yeast_500


        Skyline peak picking reference format:
        PeptideSequence,Peptide,FileName,PeptideRetentionTime,MinStartTime,MaxEndTime
        FQYIAISQSDADSESCK,FQYIAISQSDADSESCK,Q_2013_0129_RJ_11.raw,38.36,38.09,38.6
        FQYIAISQSDADSESCK,FQYIAISQSDADSESCK,Q_2013_0129_RJ_20.raw,38.46,38.19,38.6
        FQYIAISQSDADSESCK,FQYIAISQSDADSESCK,Q_2013_0129_RJ_25.raw,38.33,38.06,38.64

        ''', 
        prog = 'PecanVisulize.py',
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-l', dest='list', help='path to a list of peak picking results\ntwo column: file and label\n')
    parser.add_argument('-f', dest='file', help='path to the peak picking result (plotting from single file/experiment)\n')
    parser.add_argument('-a', dest='label', type=str, help='label for the file (only used with -f)\n')
    parser.add_argument('-t', dest='figureTitle', help='set figure title\n')
    parser.add_argument('-q', dest='qValue', type=float, default=1.1, help='q-value cutoff\n')
    parser.add_argument('-e', dest='extension', type=str, default='raw')
    parser.add_argument('--proteinTag', type=str, default=None,
                        help='filter percolator results to only those with proteinIds containing this tag')

    parser.add_argument('-r', dest='reference', type=str, help='path to RT reference csv\n')
    parser.add_argument('--noplot', action='store_true', help='skip any plotting\n')
    parser.add_argument('-o', dest='outputRoot', type=str, help='name base of the output pdf file\n')
    parser.add_argument('--ymin', type=float, default=0.5, help='set y min of ROC curve (default = 0.5)\n') 
    parser.add_argument('--ymax', type=float, default=1.0, help='set y man of ROC curve (default = 1.0)\n')
    parser.add_argument('--xmin', type=float, default=0.0, help='set y min of ROC curve (default = 0.0)\n')
    parser.add_argument('--xmax', type=float, default=1.0, help='set y man of ROC curve (default = 0.5)\n')
    parser.add_argument('--printIncorrects', action='store_true', help='print incorrect picks to standard err\n')
    parser.add_argument('--multipleTopN', type=int, default=None,
                        help='Up to topN peaks. Need to have corresponding percolator result of xxx.feature.top_1.rslt'
                             '\nDefault=Not considering it\n')

    parser.add_argument('--fileType', type=str, default='rslt', help='input file type is? (rslt/feature)\n\n')

    args = parser.parse_args()
        
    File = collections.namedtuple('File', 'path label')
    Peak = collections.namedtuple('Peak', 'PeptideSequence FileName RetentionTime Score')
    PeakBoundary = collections.namedtuple('PeakBoundary', 'MinStartTime MaxEndTime')
    
    files2plot = []
    
    if args.file:
        if args.label:
            files2plot.append(File(args.file, args.label))
        else:
            files2plot.append(File(args.file, ""))
    elif args.list:
        listFile = csv.reader(open(args.list, 'rb'), delimiter=',')
        for row in listFile:
           files2plot.append(File(row[0], str(row[1])))
    else:
        sys.stderr.write("Either -f FILE or -l LIST is needed\n")
        sys.exit()

    Data = {}  # Key = file.label, Values = [peaks]
    exps = []
    for file in files2plot:
        if args.fileType == 'rslt':
            if file.label in Data:
                Data[file.label] += readPercolatorFile(file.path, args.extension, args.qValue, protTag=args.proteinTag)
            else:
                Data[file.label] = readPercolatorFile(file.path, args.extension, args.qValue, protTag=args.proteinTag)
                exps.append(file.label)
        elif args.fileType == 'feature':
            if file.label in Data:
                Data[file.label] += readPecanFeatures(file.path, args.extension, topNcutoff=10, protTag=args.proteinTag)
            else:
                Data[file.label] = readPecanFeatures(file.path, args.extension, topNcutoff=10, protTag=args.proteinTag)
                exps.append(file.label)
        else:
            sys.stderr.write('Unknown fileType %s\n' % args.fileType)
            sys.exit()

    rawFileUsed = []
    for exp in Data:
        for peak in Data[exp]:
            if peak.FileName not in rawFileUsed:
                rawFileUsed.append(peak.FileName)
    print 'rawFileUsed: %s' % rawFileUsed

    peakReference = {}        # key = peptide, value = {} where key=file, value=PeakBoundary [minStartTime, maxEndTime]
    peakReferenceCount = {}     # key = expFileName, value = number of valid reference peaks
    peakReferenceTotal = {}     # key = expFileName, value = number of total reference peaks

    if args.reference:
        sys.stderr.write("===== Reading reference file %s\n" % args.reference )
        peakReferenceFile = csv.DictReader(open(args.reference, 'rb'), delimiter=',')
        for row in peakReferenceFile:
            pepSeq = row.get('PeptideSequence')
            expFileName = row.get('FileName')
            if expFileName not in rawFileUsed:
                continue
            if pepSeq in peakReference:
                if expFileName not in peakReference[pepSeq]:
                    peakReference[pepSeq][expFileName] = PeakBoundary(row.get('MinStartTime'), row.get('MaxEndTime'))
            else:
                peakReference[pepSeq] = {}
                peakReference[pepSeq][expFileName] = PeakBoundary(row.get('MinStartTime'), row.get('MaxEndTime'))

        for pepSeq in peakReference:
            for expFileName in peakReference[pepSeq]:
                if peakReference[pepSeq][expFileName].MinStartTime != "#N/A":
                    if expFileName in peakReferenceCount:
                        peakReferenceCount[expFileName] += 1
                    else:
                        peakReferenceCount[expFileName] = 1
                if expFileName in peakReferenceTotal:
                    peakReferenceTotal[expFileName] += 1
                else:
                    peakReferenceTotal[expFileName] = 1
    else:
        sys.stderr.write("reference file is needed\n")
        sys.exit(1)
    for expFileName in sorted(peakReferenceCount.keys(), reverse=True):
        sys.stdout.write("File: %s has %s/%s reference peaks\n" % (expFileName, peakReferenceCount[expFileName], peakReferenceTotal[expFileName]))

    totalRefPeaks = 0
    validRefPeaks = 0
    for pepSeq in peakReference:
        for file in peakReference[pepSeq]:
            if peakReference[pepSeq][file].MinStartTime != "#N/A":
                validRefPeaks += 1
            totalRefPeaks += 1

    sys.stderr.write('Total reference peaks: %s/%s\n' % (validRefPeaks, totalRefPeaks))

    ### plotting multiple topN results
    TopNData = {}   # Key = topN, Values = {key = exp, value=[peaks]}
    TopNStats = {}
    if args.multipleTopN is not None and args.fileType == 'feature':
        topNs = range(1, args.multipleTopN+1)
        for n in topNs:
            TopNData[n] = {}
            result = []
            for f in files2plot:
                #topNFileName = '%s.top_%s.LOO21.rslt' % (f.path, n)
                topNFileName = '%s.top_%s.rslt' % (f.path, n)
                exp = f.label
                try:
                    result = readPercolatorFile(topNFileName, args.extension, qValueCutoff=0.01, protTag=args.proteinTag)
                    if exp in TopNData[n]:
                        TopNData[n][exp] += result
                    else:
                        TopNData[n][exp] = result
                except IOError:
                    pass

        for n in TopNData:
            TopNStats[n] = {}
            for exp in TopNData[n]:
                stats = calPeakStats(TopNData[n][exp], peakReference)[0]
                TopNStats[n][exp] = stats
    ###

    if args.outputRoot:
        pdfFileName = '%s.PV.pdf' % args.outputRoot
    elif args.list:
        pdfFileName = '%s.PV.pdf' % re.sub('.txt', '', args.list)
    elif args.file:
        pdfFileName = '%s.PV.pdf' % re.sub('.[rslt,feature]', '', args.file)
    else:
        pdfFileName = 'multipage.pdf'


    ''' plot parameters setting '''
    fontSize = 8
    matplotlib.rcParams.update({'font.size': fontSize})
    colorCycle = [
                '#0000ff',
                '#8a2be2',
                '#a52a2a',
                '#d2691e',
                '#008b8b',
                '#008000',
                '#ffa500',
                '#7570b3',
                '#800080',
                '#32cd32',
                '#ffd700',
                '#ff00ff',
                '#800000',
                '#6b8e23',
                '#4169e1',
                '#0000ff',
                '#8a2be2',
                '#a52a2a',
                '#d2691e',
                '#008b8b'
                ]

    matplotlib.rcParams['axes.color_cycle'] = colorCycle
   
    ''' plot setting '''
    #pp = PdfPages(pdfFileName)

    if args.fileType == 'rslt':
        fig = plt.figure()
        ax1 = fig.add_subplot(221)
        #ax1 = plt.subplot2grid((2,2),(0, 0), colspan=1)
        if args.figureTitle:
            ax1.set_title(args.figureTitle)
        ax1.set_xlim([args.xmin, args.xmax])
        ax1.set_ylim([args.ymin, args.ymax])
        formatter = FuncFormatter(to_percent)
        ax1.yaxis.set_major_formatter(formatter)
        ax1.grid(b=True, which='major', color='gray', linestyle='--')
        ax1.set_xlabel('Pecan q-value')
        ax1.set_ylabel('Fraction of correct / picked peaks')
        ax1.set_axisbelow(True)

        ax2 = fig.add_subplot(222)
        #ax2 = plt.subplot2grid((2,2),(1, 0), colspan=1)
        if args.figureTitle:
            ax2.set_title(args.figureTitle)
        ax2.set_xlim([0.6, args.xmax])
        ax2.set_ylim([0.6, args.ymax])
        #formatter = FuncFormatter(to_percent)
        #ax2.yaxis.set_major_formatter(formatter)
        ax2.grid(b=True, which='major', color='gray', linestyle='--')
        #ax2.set_xlabel('Observed FDR')
        ax2.set_xlabel('Fraction of correct / reference peaks')
        ax2.set_ylabel('Fraction of correct / picked peaks')
        ax2.set_axisbelow(True)

        ''' Q-Q plot setting '''
        ax3 = fig.add_subplot(223)
        #ax3 = plt.subplot2grid((2,2),(0, 1), rowspan=1)
        ax3.set_xscale('log')
        ax3.set_yscale('log')
        ax3.set_xlim([0.001, 1])
        ax3.set_ylim([0.001, 1])
        ax3.set_xlabel('Pecan q-value')
        ax3.set_ylabel('Observed FDR')
        ax3.plot([0.001,1],[0.001,1], color='gray', linestyle='--')
        ax3.set_axisbelow(True)

        ax4 = fig.add_subplot(224)
        if args.figureTitle:
            ax4.set_title(args.figureTitle)
        ax4.set_xlim([args.xmin, args.xmax])
        ax4.set_ylim([args.ymin, args.ymax])
        formatter = FuncFormatter(to_percent)
        ax4.yaxis.set_major_formatter(formatter)
        ax4.grid(b=True, which='major', color='gray', linestyle='--')
        ax4.set_xlabel('Pecan q-value')
        ax4.set_ylabel('Fraction of correct / reference peaks')
        ax4.set_axisbelow(True)

        ''' processing and plotting every flie '''
        QQs = []
        maximum = []

        bins = np.array(range(0, 101, 1))/100.0

        for exp in exps:
            stats, correctScores, incorrectScores = calPeakStats( Data[exp], peakReference)

            correctHist, bin_edges = np.histogram(correctScores, bins=bins)
            if args.noplot:
                continue

            maximum.append(stats.correctPeaks)
            correctCumulative = np.cumsum(correctHist, dtype=np.float)/stats.totalPeaks
            #print correctCumulative

            FDRs = calculateFDR(correctScores, incorrectScores)
            correctPickFDRs = []
            for i in correctScores:
                correctPickFDRs.append(FDRs[i])

            FDRHist, bin_edges = np.histogram(correctPickFDRs, bins=bins)
            FDRCumulative = np.cumsum(FDRHist, dtype=np.float)/stats.totalPeaks
            #print FDRCumulative

            correctCumulativeRef = np.cumsum(correctHist, dtype=np.float)/totalRefPeaks

            qValues = []
            oqValues = []
            for q in sorted(FDRs, key=lambda key: FDRs[key]):
                qValues.append(q)
                oqValues.append(FDRs[q])

            ax1.plot(bins[:-1], correctCumulative, label=exp, linewidth=1.5)
            #ax2.plot(bins[:-1], FDRCumulative, label=exp, linewidth=1.5)
            x_sum = max(correctCumulativeRef)
            y_sum = max(correctCumulative)
            ax2.plot(x_sum, y_sum, "o", label=exp, markersize=5)
            print x_sum, y_sum
            ax3.plot(qValues, oqValues, label=exp)
            ax4.plot(bins[:-1], correctCumulativeRef, label=exp, linewidth=1.5)

        # or sort them by
        handles, labels = ax1.get_legend_handles_labels()
        if len(labels) >= 2:
            hl = sorted(zip(handles, labels, maximum), key=operator.itemgetter(2), reverse=True)
            handles, labels, orders = zip(*hl)

        ax1.legend(handles, labels, loc=4, prop={'size': fontSize-1})
        #ax2.legend(handles, labels, loc=4, prop={'size': fontSize-1})
        ax3.legend(handles, labels, loc=4, prop={'size': fontSize-1})
        ax4.legend(handles, labels, loc=4, prop={'size': fontSize-1})

    elif args.fileType == 'feature':
        fig = plt.figure()
        #ax1 = fig.add_subplot(221)
        ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=1, rowspan=1)
        if args.figureTitle:
            ax1.set_title(r'%s' % args.figureTitle)
        ax1.set_xlim([args.xmin, args.xmax])
        ax1.set_ylim([args.ymin, args.ymax])
        #formatter = FuncFormatter(to_percent)
        #ax1.yaxis.set_major_formatter(formatter)
        ax1.grid(b=True, which='major', color='gray', linestyle='--')
        ax1.set_xlabel('1-specificity')
        ax1.set_ylabel('sensitivity')
        ax1.set_axisbelow(True)

        #ax2 = fig.add_subplot(222)

        maximum = []
        for exp in exps:
            sensitivities = [0.0]
            specificities = [1.0]
            for i in xrange(1, 4):
                peakList = [peak for peak in Data[exp] if peak.TopN <= i]
                stats = calPeakStats(peakList, peakReference)[0]
                sensitivities.append(float(stats.correctPeptides)/totalRefPeaks)
                specificities.append(float(stats.correctPeaks)/stats.totalPeaks)
            sensitivities.append(1.0)
            specificities.append(0.0)
            print sensitivities
            print specificities
            x = 1-np.array(specificities)
            #ax1.plot(x, sensitivities, label=exp, linewidth=1.5)
            auc = np.trapz(sensitivities, x)
            #thisLabel = r'AUC=%.3f $\alpha$=%s' % (auc, exp)

            thisLabel = r'$\alpha$=%s' % exp
            ax1.plot(x, sensitivities, 'o', label=thisLabel)

            maximum.append(auc)

        handles, labels = ax1.get_legend_handles_labels()
        if len(labels) >= 2:
            hl = sorted(zip(handles, labels, maximum), key=operator.itemgetter(2), reverse=True)
            handles, labels, orders = zip(*hl)

        ax1.legend(handles, labels, prop={'size': fontSize-1}, loc='center left', bbox_to_anchor=(1.1, 0.5))

        # plotting the bar plot
        if args.multipleTopN is not None:
            ax2 = plt.subplot2grid((2, 2), (1, 0), colspan=2, rowspan=1)
            #ax2 = plt.subplot2grid((2, 2), (1, 0), colspan=1, rowspan=1)
            if args.figureTitle:
                ax2.set_title(r'%s' % args.figureTitle)
            ax2.grid(b=True, which='major', color='gray', linestyle='--')
            ax2.set_ylabel('Number of peptide detected\n(q-value<0.01)')
            ax2.set_xlabel(r'$\alpha$')
            #ax2.set_xticks( np.array(xrange(len(exps)))*(len(topNs)+1)+1 )
            #ax2.set_xticklabels(exps, rotation=90)
            ax2.set_xticklabels(exps)
            ax2.set_axisbelow(True)
            i = 0
            barWidth = 0.2
            shifts = [0, 0.8, 1.6, 2.4, 3.2]
            ticksPosition = np.array(xrange(len(exps)))*((len(topNs))+1)+1+0.8+barWidth
            print ticksPosition
            ax2.set_xticks(ticksPosition)

            for exp in exps:
                positions = np.array([shifts[s] for s in xrange(0, len(topNs))])+(len(topNs)+1)*i+1-(barWidth/2.0)
                print positions
                y = []
                for n in topNs:
                    stats = TopNStats[n][exp]
                    pepID = stats.correctPeaks
                    print n, exp, pepID, stats.correctPeptides, stats.totalPeaks
                    y.append(pepID)
                plt.bar(positions, y, label=n, color=colorCycle[i])
                i += 1
            '''
            i = 0
            for n in topNs:
                positions = np.array(xrange(len(exps)))*5.0+1+shifts[i]-(barWidth/2.0)
                print positions
                y = []
                for exp in exps:
                    stats = TopNStats[n][exp]
                    pepID = stats.correctPeaks
                    print n, exp, pepID, stats.correctPeptides, stats.totalPeaks
                    y.append(pepID)
                plt.bar(positions, y, label=n)
                i += 1
            '''
    else:
        sys.stderr.write('Unknown fileType %s\n\n' % args.fileType)



    fig.set_tight_layout(True)
    svgFileName = re.sub('.pdf','.svg',pdfFileName)
    plt.savefig(svgFileName, format='svg')
    #pp.savefig()
    #pp.close()

    sys.exit()




    



